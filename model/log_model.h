#ifndef Log_Model_H
#define Log_Model_H
#include "stdio.h"
#include "stdlib.h"
#include "../basic_data_structuers/data-structers/log_queue.h"
#include "../utils/constants.h"

void printLog(log_node *logArr ,int length) {
    printf("%-20s %-25s %-15s\n", "Username", "Time", "Command");
    for (int i = 0; i < length ; i++) {
        printf("%-20s %-25s %-15s\n", logArr[i].name, asctime(logArr[i].time), logArr[i].command);
    }
}

void write_to_log_file(queue_log *data) {
    FILE *fp = fopen("../doc/log.txt", "ab");
    if (fp == NULL) {
        perror("Error opening file");
        return;
    }

    int offset = 0;
    while (offset < data->cnt) {
        // 使用 sizeof(log) 而不是 sizeof(data)
        fwrite(&(data->logArr[offset]), sizeof(log_node), 1, fp);
        offset++;
    }

    fclose(fp);

    /*
    &data + offset：这个表达式取得是整个 data 结构体指针的地址加上 offset，而不是具体队列数据的地址。这样的操作会导致指针偏移错误，
    因为 data 是指向整个队列结构的指针，不是指向队列数据的指针。

    &(data->logArr[offset])：这个表达式取得是队列中的 logArr 数组的地址加上 offset。这是正确的操作 ，
    因为 data->logArr 是指向队列数据的指针，然后加上 offset 就可以得到具体数据的地址。
    while(fwrite(&data + offset ,sizeof(log)  ,1 ,fp )==1 && i++ < data->cnt) {
        offset+=sizeof(data->logArr[i]) ;
    }
    */
}

int read_from_log_file(queue_log *queue_log ,log_node *logArr) {
    FILE *fp = fopen("../doc/log.dat" ,"rb" ) ;
    if (fp == NULL) {
        perror("Error opening file");
        return 0;
    }

    int offset = 0 ;
    /*
    log logArr[50];：在函数的栈上创建了一个局部数组 logArr，但在函数结束后，
    这个数组会被销毁。因此，不能将局部数组的地址返回给调用者。如果你想要返回一个数组，你可能需要动态分配内存，并在调用者负责释放。
    */
    /*
    在fread(&logArr[offset++], sizeof(log), 1, fp);中，offset++实际上是一个后缀自增运算符，它在&logArr[offset]的值被传递给fread
    之前增加offset的值。这会导致每次迭代中&logArr[offset]都指向数组中的下一个元素，但是offset的值在fread调用之前就已经增加了
    */
    while(fread(&logArr[offset] ,sizeof(log_node),1,fp)==1 && offset < queue_log->size) {
        offset++ ;
    }
    fclose(fp) ;

    return (offset - 1) ;
}

void addLog(queue_log *queue_log ,const char *str) {
    log_node *data = getLogNode(username  ,str) ;
    log_enqueue(queue_log ,data) ;
}

void show_Log(queue_log *queue_log) {
    log_node *logArr = (log_node *)malloc(sizeof(log_node) * queue_log->size) ;
    int length = read_from_log_file(queue_log ,logArr) ;
    printf("\n--------------------------table of Log -----------------------------------------\n") ;
    printLog(logArr ,length) ;
    printf("\n--------------------------end -----------------------------------------\n") ;
}

#endif
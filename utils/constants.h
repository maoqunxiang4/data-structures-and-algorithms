#ifndef Constants_H
#define Constants_H
#include "../basic_data_structuers/data-structers/log_queue.h"

static char *student_path = "./doc/students.dat" ;
static char *teacher_path = "./doc/teacher.dat" ;
static char *course_path = "./doc/course.dat" ;
static char *grade_path = "./doc/grade.dat" ;
static char *class_path = "./doc/class.dat" ;
static char *user_path = "./doc/user.dat" ;
static char *log_path = "./doc/log.dat" ;

char *username = "" ;

int log_queue_size = 50 ;
int grade_hash_table_size = 50 ;
int MAX_LEVEL = 20 ;


#endif
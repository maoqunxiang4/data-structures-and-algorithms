#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #include "../user_data_structers/user_data_structers.h"
// #include "../utils/write_basic_info.h"
// #include "../utils/read_basic_info.h"
// #include "constants.h"

#include <stdio.h>

typedef struct {
    int id;
    int age;
    int grade;
    char name[50];
    char gender[10];
    char enrollmentDate[20];
    int classId; // 所属班级编号
} Student;

const char *student_path = "students.txt";

void write_student_to_file(Student *student) {
    FILE *fp = fopen(student_path, "a"); // 使用追加模式，可以在文件末尾添加新的学生信息
    if (fp == NULL) {
        printf("Failed to open file %s\n", student_path);
        return;
    }
    fwrite(student, sizeof(Student), 1, fp);
    fclose(fp);
}

void read_students_from_file() {
    FILE *fp = fopen(student_path, "r");
    if (fp == NULL) {
        printf("Failed to open file %s\n", student_path);
        return;
    }

    Student student;

    printf("Reading from file:\n");
    while (fread(&student, sizeof(Student), 1, fp) == 1) {
        printf("name: %s\n", student.name);
        printf("age: %d\n", student.age);
        printf("grade: %d\n", student.grade);
        printf("\n");
    }

    fclose(fp);
}

int main() {
    printf("Enter information for 10 students:\n");

    Student student ;
    // 手动输入10个学生信息
    for (int i = 1; i <= 2; ++i) {
        printf("Student ID: ");
        scanf("%d", &student.id);

        printf("Age: ");
        scanf("%d", &student.age);

        printf("Grade: ");
        scanf("%d", &student.grade);

        printf("Name: ");
        scanf("%s", student.name);

        printf("Gender: ");
        scanf("%s", student.gender);

        printf("Enrollment Date: ");
        scanf("%s", student.enrollmentDate);

        printf("Class ID: ");
        scanf("%d", &student.classId);

        write_student_to_file(&student);
    }

    read_students_from_file();

    return 0;
}
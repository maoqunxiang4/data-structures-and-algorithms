#ifndef Write_Basic_Info_H
#define Write_Basic_Info_H
#include "../user_data_structers/user_data_structers.h"
#include "constants.h"
#include "stdio.h"
#include "stdlib.h"

// 写入学生信息到文件
// 写入学生信息到文件
void write_student_to_file(Student *student) {
    FILE *fp = fopen(student_path, "ab");
    fseek(fp, 0, SEEK_END);
    if (fp == NULL) {
        printf("Failed to open file %s\n", student_path);
        return;
    }
    fwrite(student, sizeof(Student), 1, fp);
    fclose(fp);
}

void write_teacher_to_file(const Teacher *teacher) {
    char *filename = teacher_path ;
    FILE *fp = fopen(teacher_path ,"ab") ;
    fseek(fp, 0, SEEK_END);
    if(fp == NULL) {
        printf("the file is NULL") ;
    }
    fwrite(teacher ,sizeof(Teacher) , 1 , fp) ;
    fclose(fp);
}

void write_grade_to_file(const Grade *grade) {
    char *filename = grade_path ;
    FILE *fp = fopen(grade_path ,"ab") ;
    fseek(fp, 0, SEEK_END);
    if(fp == NULL) {
        printf("the file is NULL") ;
    }
    fwrite(grade ,sizeof(Grade) , 1 , fp) ;
    fclose(fp);
}

void write_course_to_file(const Course *course) {
    char *filename = course_path ;
    FILE *fp = fopen(course_path ,"ab") ;
    fseek(fp, 0, SEEK_END);
    if(fp == NULL) {
        printf("the file is NULL") ;
    }
    fwrite(course ,sizeof(Course) , 1 , fp) ;
    fclose(fp);
}

#endif
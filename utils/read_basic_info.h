#ifndef Read_Basic_Info_H
#define Read_Basic_Info_H
#include "../user_data_structers/user_data_structers.h"
#include "stdio.h"
#include "stdlib.h"
#include "constants.h"

void read_students_from_file() {
    FILE *fp = fopen(student_path, "rb");
    if (fp == NULL) {
        printf("Failed to open file %s\n", student_path);
        return;
    }

    Student student;

    while (fread(&student, sizeof(Student), 1, fp) == 1) {
            printf("name:%s\n", student.name);
            printf("age:%d\n", student.age);
            printf("grade:%d\n", student.grade);
            printf("classId:%d\n", student.classId);
            printf("\n");
    }

    fclose(fp);
}

void read_teacher_info() {
    FILE *fp = fopen(teacher_path , "rb") ;
    if(fp==NULL) {
        printf("the file is not exit") ;
    }

    Teacher teacher ;
    while( (fread(&teacher , sizeof(Teacher) , 1 ,fp)) == 1) {
        if(teacher.age < 100) {
            printf("name:%s\n" ,teacher.name) ;
            printf("age:%d\n" ,teacher.age) ;
            printf("classId:%d\n" ,teacher.classId) ;
        }
    }

    fclose(fp) ;
}

void read_grade_info(const char *filename) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        printf("Failed to open file %s\n", filename);
        return;
    }

    Grade grade;
    while (fread(&grade, sizeof(Grade), 1, fp) == 1) {
        printf("Student ID: %d\n", grade.studentId);
        printf("Course Name: %d\n", grade.coursename);
        printf("Score: %.2f\n", grade.score);
        printf("\n");
    }

    fclose(fp);
}

#endif
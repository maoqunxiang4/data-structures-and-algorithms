#ifndef User_Data_Structers_H
#define User_Data_Structers_H

// 学生信息结构体
typedef struct {
    int id;
    int age ;
    int grade ;
    char name[50];
    char gender[10];
    char enrollmentDate[20];
    int classId; // 所属班级编号
} Student;

// 教师信息结构体
typedef struct {
    int id;
    int age ;
    char name[50];
    char gender[10];
    char hireDate[20];
    char subject[50];
    int classId; // 所属班级编号
} Teacher;

// 学生成绩信息结构体   
typedef struct {
    char studentName[50];
    int studentId;
    char coursename[50];
    float score;
} Grade; 

// 用户信息结构体   
typedef struct {
    char username[50];
    char password[50];
    char role[10];
} User;

//课程信息结构体   
typedef struct {
    int courseId;
    char courseName[50];
    int credit;
    char teacherName[50] ;
} Course ;

//权限结构体   
typedef struct {
    char role[10] ;
    char action[50];
} auth ;
#endif
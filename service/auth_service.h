#ifndef Auth_Service_H
#define Auth_Service_H
#include "../basic_data_structuers/data-structers/auth_skipList.h"
#include "../utils/constants.h"
#include <stdbool.h>

/*
权限类型：查询权限，添加权限，删除权限，修改权限，查看日志的权限，
学生：查询课程信息，查询学生的信息，查询个人成绩
老师：添加、删除、修改、查询学生信息；查询课程信息；录入、删除、修改、查询学生成绩，生成班级成绩排名
管理员：添加、删除、修改、查询学生信息；添加、删除、修改、查询教师信息；添加、删除、修改、查询
课程信息；录入、删除、修改、查询学生成绩；生成班级成绩排名；查看用户操作日志信息；添加成员信息

低级查询权限：查询课程信息，查询学生的信息，查询个人成绩
中级查询权限：查询课程信息，查询学生的信息，查询个人成绩，查询整体成绩信息
高级查询权限：可以查询所有的信息

低级添加、删除、修改权限：
高级添加、删除、修改权限：

特权权限：查看用户操作日志信息；添加成员信息

Student（学生）：
低级查询权限

Teacher（老师）：
高级查询权限（针对学生信息，课程信息，学生成绩）
低级操作权限（录入、删除、修改学生成绩）

Admin（管理员）：
高级查询权限
高级操作权限
特权权限（查看用户操作日志信息，添加成员信息）
*/

// Define permission constants
const auth LOW_STUDENT_PERMISSIONS[] = {
    {"Student", "Low Query"}
};

const auth LOW_TEACHER_PERMISSIONS[] = {
    {"Student", "Low Query"} ,
    {"Teacher", "High Query"},
    {"Teacher", "Low Operation"}
};

const auth ADMIN_PERMISSIONS[] = {
    {"Student", "Low Query"} ,
    {"Teacher", "Low Operation"} ,
    {"Admin", "High Query"},
    {"Admin", "High Operation"},
    {"Admin", "Privileged"}
};

// Assign permissions to a role in the skip list
void assignPermissions(SkipList* s, const char* role, const auth permissions[], int size) {
    for (int i = 0; i < size; ++i) {
        auth auth_data = permissions[i];
        strcpy(auth_data.role, role);
        auth_insert(s, auth_data);
    }
}

// Check if a role has a specific permission in the skip list
bool auth_hasPermission(SkipList* s, const char* role, const char* action) {
    auth auth_data;
    strcpy(auth_data.role, role);
    strcpy(auth_data.action, action);

    SkipNode* p = auth_search(s, auth_data.role);
    return p != NULL && strcmp(p->data.action, action) == 0;
}

#endif
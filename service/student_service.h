#ifndef Student_Service_H
#define Student_Service_H
#include "../utils/read_basic_info.h"
#include "../utils/write_basic_info.h"
#include "../basic_data_structuers/data-structers/student_avl.h"
#include "../utils/constants.h"
#include "../model/log_model.h"

// enum student_data {
//     id ,
//     age  ,
//     grade ,
//     name ,
//     gender ,
//     enrollmentDate ,
//     classId
// } ;

queue_singleton *student_queue_instance = NULL ;
student_avl_node *student_root = STU_NIL ;

student_avl_node *initStudentInfo()  ;

void setStudentQueueinstance() {
    student_queue_instance = getQueueSingleton() ;
}

void showStudentsInfo() {
    if (student_root == STU_NIL)
        return;

    printf("+------+-----+-------+-------------------------+--------+-------------------+----------+\n");
    printf("|  ID  | Age | Grade |          Name           | Gender | Enrollment Date  | Class ID |\n");
    printf("+------+-----+-------+-------------------------+--------+-------------------+----------+\n");

    student_inOrder(student_root);

    printf("+------+-----+-------+-------------------------+--------+-------------------+----------+\n\n\n\n");
}

void showStudentInfo(Student student) {
    printf("\n------------------------student information-------------------------------\n") ;
    printf("ID: %d\n", student.id);
    printf("Age: %d\n", student.age);
    printf("Grade: %d\n", student.grade);
    printf("Name: %s\n", student.name);
    printf("Gender: %s\n", student.gender);
    printf("Enrollment Date: %s\n", student.enrollmentDate);
    printf("Class ID: %d\n", student.classId);
    printf("\n------------------------student information-------------------------------\n") ;
}

student_avl_node *inputStudentInfo(Student student) {
    printf("Enter student information:\n");

    printf("Student ID: ");
    scanf("%d", &student.id);

    printf("Age: ");
    scanf("%d", &student.age);
    
    printf("Grade: ");
    scanf("%d", &student.grade);
    
    printf("Name: ");
    scanf("%s", student.name);
    
    printf("Gender: ");
    scanf("%s", student.gender);

    printf("Enrollment Date: ");
    scanf("%s", student.enrollmentDate);

    printf("Class ID: ");
    scanf("%d", &student.classId);

    printf("make sure the infomation of the student is right .");
    showStudentInfo(student) ;
    printf("if you want to undo ,input \"undo\" else you can input \"sure\":") ;

    char command[10];
    scanf("%s" ,command) ;
    while(strcmp(command ,"sure")!=0&&strcmp(command ,"undo")!=0) {
        printf("please input again...") ;
        scanf("%s" ,command) ;
    }

    if(strcmp(command ,"sure")==0) {
        write_student_to_file(&student) ;
        student_root = student_avl_insert(student_root , student) ;
        return student_root ;
    }

    else if(strcmp(command ,"undo")) 
        return student_root;
}


//初始化学生信息
student_avl_node *initStudentInfo() {
    student_root = getStudentTreeSingleton()->root ;

    FILE *fp = fopen(student_path ,"rb") ;
    Student student ;
    while(fread(&student ,sizeof(Student) ,1 ,fp)) {
        if(student.id!=0) 
            student_root = student_avl_insert(student_root ,student) ;
    }
    
    return student_root ;
}

//添加新学生
student_avl_node *addNewStudent() {
    Student student ;
    //记录用户操作
    student_root = inputStudentInfo(student) ;
    addLog(student_queue_instance->queue_log ,"add a student information") ;
    return student_root ;
}

//删除学生
student_avl_node *deleteStudent(int id) {
    FILE *fp = fopen(student_path , "r+b") ;
    if(fp==NULL) {
        printf("the file is not exist here") ;
    }

    int offset = 0 ;
    Student student ;
    while(fread(&student ,sizeof(Student) , 1 ,fp) ==1 ) {
        if(student.id == id) break ;
        offset++ ;
    }
    fseek(fp ,sizeof(Student) * offset , SEEK_SET) ;

    Student emptyStudent = {0} ;
    fwrite(&emptyStudent ,sizeof(Student) ,1 ,fp) ;
    fclose(fp) ;

    student_root = student_avl_erase(student_root ,student.id) ;
    addLog(student_queue_instance->queue_log ,"delete a student information") ;
    return student_root ;
}

//查询新学生
Student searchStudent(student_avl_node *root ,int student_id) {
    addLog(student_queue_instance->queue_log ,"seacrh a student information") ;
    return student_avl_search(root,student_id) ;
}

//修改学生信息
student_avl_node *changeStudentInfo(int id) {
    FILE *fp = fopen(student_path , "r+b") ;
    if(fp==NULL) {
        printf("the file is not exist here") ;
    }

    int offset = 0 ;
    Student student ;
    while(fread(&student ,sizeof(Student) , 1 ,fp) ==1 ) {
        if(student.id == id) break ;
        offset++ ;
    }
    fseek(fp ,sizeof(Student) * offset , SEEK_SET) ;

        printf("Enter student information:\n");

    printf("Student ID: ");
    scanf("%d", &student.id);

    printf("Age: ");
    scanf("%d", &student.age);
    
    printf("Grade: ");
    scanf("%d", &student.grade);
    
    printf("Name: ");
    scanf("%s", student.name);
    
    printf("Gender: ");
    scanf("%s", student.gender);

    printf("Enrollment Date: ");
    scanf("%s", student.enrollmentDate);

    printf("Class ID: ");
    scanf("%d", &student.classId);

    printf("make sure the infomation of the student is right .");
    showStudentInfo(student) ;
    printf("if you want to undo ,input \"undo\" else you can input \"sure\":") ;
    
    fwrite(&student ,sizeof(Student) ,1 ,fp) ;
    fclose(fp) ;
    student_root = student_avl_erase(student_root ,student.id) ;
    student_root = student_avl_insert(student_root , student) ;
    addLog(student_queue_instance->queue_log ,"change a student information") ;
    return student_root ;
}



#endif
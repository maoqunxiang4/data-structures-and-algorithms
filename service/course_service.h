#ifndef Course_Service_H
#define Course_Service_H

#include "../utils/read_basic_info.h"
#include "../utils/write_basic_info.h"
#include "../basic_data_structuers/data-structers/course_avl.h"
#include "../utils/constants.h"
#include "../model/log_model.h"

enum course_data {
    courseId,
    courseName,
    credit,
    teacherName
};

queue_singleton *course_queue_instance = NULL;
course_avl_node *cour_root = COU_NIL ;

void setCourseQueueInstance() {
    course_queue_instance = getQueueSingleton();
}

void showCourseInfo(Course course) {
    printf("\n------------------------Course Information-------------------------------\n");
    printf("Course ID: %d\n", course.courseId);
    printf("Course Name: %s\n", course.courseName);
    printf("Credit: %d\n", course.credit);
    printf("Teacher Name: %s\n", course.teacherName);
    printf("\n------------------------Course Information-------------------------------\n");
}

course_avl_node *inputCourseInfo(Course course) {
    printf("Enter course information:\n");

    printf("Course ID: ");
    scanf("%d", &course.courseId);

    printf("Course Name: ");
    scanf("%s", course.courseName);

    printf("Credit: ");
    scanf("%d", &course.credit);

    printf("Teacher Name: ");
    scanf("%s", course.teacherName);

    printf("Make sure the information of the course is correct.\n");
    showCourseInfo(course);

    printf("If you want to proceed, input 'sure'; else, input 'undo': ");

    char command[10];
    scanf("%s", command);
    while (strcmp(command, "sure") != 0 && strcmp(command, "undo") != 0) {
        printf("Please input 'sure' or 'undo': ");
        scanf("%s", command);
    }

    if (strcmp(command, "sure") == 0) {
        // Write course information to file or AVL tree (you need to implement this)
        // write_course_to_file(&course);
        cour_root = course_avl_insert(cour_root,course) ;
        addLog(course_queue_instance->queue_log, "add a course information");
    } else if (strcmp(command, "undo") == 0) {
        printf("Operation canceled.\n");
    }
    return cour_root ;
}

// Add new course
course_avl_node *addNewCourse() {
    Course course;
    // Record user operation
    return inputCourseInfo(course);
}

//初始化课程信息
course_avl_node *initCourseInfo() {
    cour_root = getCourseSingleton() ;

    FILE *fp = fopen(course_path ,"rb") ;
    Course course ;
    while(fread(&course ,sizeof(Course) ,1 ,fp)) {
        if(course.courseId!=0) cour_root = course_avl_insert(cour_root ,course) ;
    }
    
    return cour_root ;
}

//删除课程
course_avl_node *deleteCourse(int id) {
    FILE *fp = fopen(course_path , "r+b") ;
    if(fp==NULL) {
        printf("the file is not exist here") ;
    }

    int offset = 0 ;
    Course course ;
    while(fread(&course ,sizeof(Course) , 1 ,fp) ==1 ) {
        if(course.courseId == id) break ;
        offset++ ;
    }
    fseek(fp ,sizeof(Course) * offset , SEEK_SET) ;

    Course emptyCourse = {0} ;
    fwrite(&emptyCourse ,sizeof(Course) ,1 ,fp) ;
    fclose(fp) ;

    addLog(course_queue_instance->queue_log ,"delete a student information") ;
    return course_avl_erase(cour_root ,course.courseId) ;
}

// Search for a course
Course searchCourse(course_avl_node *courseAVLTree, int courseId) {
    addLog(course_queue_instance->queue_log, "search a course information");
    return course_avl_search(courseAVLTree,courseId);
}

// Change course information
course_avl_node *changeCourseInfo(course_avl_node *courseAVLTree, int courseId) {
    FILE *fp = fopen(course_path , "r+b") ;
    if(fp==NULL) {
        printf("the file is not exist here") ;
    }

    int offset = 0 ;
    Course course ;
    while(fread(&course ,sizeof(Course) , 1 ,fp) ==1 ) {
        if(course.courseId == courseId) break ;
        offset++ ;
    }
    fseek(fp ,sizeof(Course) * offset , SEEK_SET) ;
        printf("Enter course information:\n");

    printf("Course ID: ");
    scanf("%d", &course.courseId);

    printf("course name: ");
    scanf("%s", &course.courseName);
    
    printf("Grade: ");
    scanf("%d", &course.credit);
    
    printf("Name: ");
    scanf("%s", course.teacherName);
    
    printf("make sure the infomation of the student is right .");
    showCourseInfo(course) ;
    printf("if you want to undo ,input \"undo\" else you can input \"sure\":") ;
    
    fwrite(&course ,sizeof(Course) ,1 ,fp) ;
    fclose(fp) ;
    cour_root = course_avl_erase(cour_root ,courseId) ;
    cour_root = course_avl_insert(cour_root ,course) ;
    addLog(course_queue_instance->queue_log ,"change a student information") ;
    return cour_root ;
}

void showAllCoursesInfo() {
    if (cour_root == COU_NIL)
        return;

    printf("+-----------+-------------------------+--------+-------------------------+\n");
    printf("| Course ID |      Course Name        | Credit |       Teacher Name      |\n");
    printf("+-----------+-------------------------+--------+-------------------------+\n");

    course_inOrder(cour_root);

    printf("+-----------+-------------------------+--------+-------------------------+\n\n\n\n");
}

#endif

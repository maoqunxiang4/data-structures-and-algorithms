#include <stdio.h>
#include "auth_service.h"

int main() {
    SkipList* skipList = createSkipList();

    // Assign permissions to different roles
    assignPermissions(skipList, "Student", LOW_STUDENT_PERMISSIONS, sizeof(LOW_STUDENT_PERMISSIONS) / sizeof(auth));
    assignPermissions(skipList, "Teacher", LOW_TEACHER_PERMISSIONS, sizeof(LOW_TEACHER_PERMISSIONS) / sizeof(auth));
    assignPermissions(skipList, "Admin", ADMIN_PERMISSIONS, sizeof(ADMIN_PERMISSIONS) / sizeof(auth));

    // Test if permissions are correctly assigned
    printf("Checking permissions:\n");
    printf("Student - Low Query: %s\n", auth_hasPermission(skipList, "Student", "Low Query") ? "Granted" : "Denied");
    printf("Teacher - High Query: %s\n", auth_hasPermission(skipList, "Teacher", "High Query") ? "Granted" : "Denied");
    printf("Admin - Privileged: %s\n", auth_hasPermission(skipList, "Admin", "Privileged") ? "Granted" : "Denied");
    printf("Admin - High Operation: %s\n", auth_hasPermission(skipList, "Admin", "High Operation") ? "Granted" : "Denied");

    // Test if invalid permissions are correctly denied
    printf("Invalid Permissions:\n");
    printf("Student - High Query: %s\n", auth_hasPermission(skipList, "Student", "High Query") ? "Granted" : "Denied");
    printf("Teacher - Low Operation: %s\n", auth_hasPermission(skipList, "Teacher", "Low Operation") ? "Granted" : "Denied");
    printf("Admin - Low Query: %s\n", auth_hasPermission(skipList, "Admin", "Low Query") ? "Granted" : "Denied");

    // Free allocated memory
    freeSkipList(skipList);

    return 0;
}

// #include <stdio.h>
// #include <string.h>

// // 教师信息结构体
// typedef struct {
//     int id;
//     int age;
//     char name[50];
//     char gender[10];
//     char hireDate[20];
//     char subject[50];
//     int classId; // 所属班级编号
// } Teacher;

// // 学生成绩信息结构体
// typedef struct {
//     char studentName[50];
//     int studentId;
//     char coursename[50];
//     float score;
// } Grade;

// // 课程信息结构体
// typedef struct {
//     int courseId;
//     char courseName[50];
//     int credit;
//     char teacherName[50];
// } Course;

// void write_teacher_to_file(const Teacher *teacher) {
//     FILE *fp = fopen("../doc/teacher.dat", "ab");
//     if (fp == NULL) {
//         printf("Failed to open file\n");
//         return;
//     }
//     fwrite(teacher, sizeof(Teacher), 1, fp);
//     fclose(fp);
// }

// void write_grade_to_file(const Grade *grade) {
//     FILE *fp = fopen("../doc/grade.dat", "ab");
//     if (fp == NULL) {
//         printf("Failed to open file\n");
//         return;
//     }
//     fwrite(grade, sizeof(Grade), 1, fp);
//     fclose(fp);
// }

// void write_course_to_file(const Course *course) {
//     FILE *fp = fopen("../doc/course.dat", "ab");
//     if (fp == NULL) {
//         printf("Failed to open file\n");
//         return;
//     }
//     fwrite(course, sizeof(Course), 1, fp);
//     fclose(fp);
// }

// int main() {
//     // 生成并写入教师数据
//     for (int i = 1; i <= 20; ++i) {
//         Teacher teacher;
//         teacher.id = i;
//         teacher.age = 30 + i;
//         sprintf(teacher.name, "Teacher%d", i);
//         strcpy(teacher.gender, (i % 2 == 0) ? "Male" : "Female");
//         sprintf(teacher.hireDate, "2022-01-01");
//         sprintf(teacher.subject, "Subject%d", i);
//         teacher.classId = i % 3 + 1;

//         write_teacher_to_file(&teacher);
//     }

//     // 生成并写入学生成绩数据
//     for (int i = 1; i <= 40; ++i) {
//         Grade grade;
//         sprintf(grade.studentName, "Student%d", i);
//         grade.studentId = i;
//         sprintf(grade.coursename, "Course%d", i % 5 + 1);
//         grade.score = 60 + (i % 40); // 假设分数在60-99之间

//         write_grade_to_file(&grade);
//     }

//     // 生成并写入课程数据
//     for (int i = 1; i <= 5; ++i) {
//         Course course;
//         course.courseId = i;
//         sprintf(course.courseName, "Course%d", i);
//         course.credit = 2 * i;
//         sprintf(course.teacherName, "Teacher%d", i);

//         write_course_to_file(&course);
//     }

//     printf("Test data written to files successfully.\n");

//     return 0;
// }

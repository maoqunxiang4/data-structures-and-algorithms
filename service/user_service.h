#ifndef User_Service_H
#define User_Service_H

#include "../utils/read_basic_info.h"
#include "../utils/write_basic_info.h"
#include "../basic_data_structuers/data-structers/grade_hash.h"
#include "../utils/constants.h"
#include "../model/log_model.h"

// 用户信息结构体   
typedef struct UserNode {
    User user;
    struct UserNode* next;
} UserNode;

UserNode* users_head = NULL;  // 用户链表头指针

// UserNode* initUserList() {
//     // 初始化用户链表
//     // 省略动态加载用户信息的代码
//     return NULL;
// }

UserNode* createUserNode(const char* username, const char* password, const char* role) {
    // 创建新的用户节点
    UserNode* newNode = (UserNode*)malloc(sizeof(UserNode));
    strcpy(newNode->user.username, username);
    strcpy(newNode->user.password, password);
    strcpy(newNode->user.role, role);
    newNode->next = NULL;
    return newNode;
}

void addUserNode(UserNode** head, const char* username, const char* password, const char* role) {
    // 将新用户节点添加到用户链表
    UserNode* newNode = createUserNode(username, password, role);
    newNode->next = *head;
    *head = newNode;
}

void read_user_info(const char *filename) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        printf("Failed to open file %s\n", filename);
        return;
    }

    User user;
    while (fread(&user, sizeof(user), 1, fp) == 1) {
        printf("Username: %s\n", user.username);
        printf("Password: %s\n", user.password);
        printf("Role: %s\n", user.role);
        printf("\n");
    }

    fclose(fp);
}

void write_user_to_file(const User *user) {
    FILE *fp = fopen(user_path, "ab");  // Open the file in binary append mode
    if (fp == NULL) {
        printf("Failed to open file %s\n", user_path);
        return;
    }

    fwrite(user, sizeof(User), 1, fp);
    fclose(fp);
}

UserNode* initUsersFromFile(const char *filename) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        printf("Failed to open file %s\n", filename);
        return NULL;
    }

    User user;
    UserNode *head = NULL;

    while (fread(&user, sizeof(user), 1, fp) == 1) {
        addUserNode(&head, user.username, user.password, user.role);
    }

    fclose(fp);
    return head;
}

UserNode* initUsers() {
    return initUsersFromFile(user_path);
}

UserNode* login() {
    char username[50];
    char password[50];

    printf("Enter your username: ");
    scanf("%s", username);
    printf("Enter your password: ");
    scanf("%s", password);

    UserNode* current = users_head;
    while (current != NULL) {
        if (strcmp(current->user.username, username) == 0 && strcmp(current->user.password, password) == 0) {
            return current;
        }
        current = current->next;
    }

    return NULL;
}

void registerUser() {
    char username[50];
    char password[50];
    char role[10];

    printf("Enter a new username: ");
    scanf("%s", username);
    printf("Enter a new password: ");
    scanf("%s", password);
    printf("Enter your role (student/teacher): ");
    scanf("%s", role);

    UserNode *newUserNode = createUserNode(username, password, role);
    addUserNode(&users_head, username, password, role);

    // Write the new user information to the file
    write_user_to_file(&(newUserNode->user));

    printf("User registered successfully.\n");
}


#endif

#ifndef Grade_Service_H
#define Grade_Service_H
#include "string.h"
#include "../utils/read_basic_info.h"
#include "../utils/write_basic_info.h"
#include "../basic_data_structuers/data-structers/grade_hash.h"
#include "../service/student_service.h"
#include "../utils/constants.h"
#include "../model/log_model.h"

// enum grade_data {
//     studentName,
//     studentId,
//     courseId,
//     score
// };

queue_singleton *grade_queue_instance = NULL;

void setGradeQueueinstance() {
    grade_queue_instance = getQueueSingleton();
}

GradeHashTable *gradeHashTable = NULL ;

void showGradeInfo(Grade grade) {
    printf("\n------------------------grade information-------------------------------\n");
    printf("Student Name: %s\n", grade.studentName);
    printf("Student ID: %d\n", grade.studentId);
    printf("Course Name: %s\n", grade.coursename);
    printf("Score: %.2f\n", grade.score);
    printf("\n------------------------grade information-------------------------------\n");
}

void inputGradeInfo( Grade grade) {
    printf("Enter grade information:\n");

    printf("Student Name: ");
    scanf("%s", grade.studentName);

    printf("Student ID: ");
    scanf("%d", &grade.studentId);

    printf("Course Name: ");
    scanf("%s", grade.coursename);

    printf("Score: ");
    scanf("%f", &grade.score);

    printf("Make sure the information of the grade is right.");
    showGradeInfo(grade);
    printf("If you want to undo, input \"undo\" else you can input \"sure\":");

    char command[10];
    scanf("%s", command);
    while (strcmp(command, "sure") != 0 && strcmp(command, "undo") != 0) {
        printf("Please input again...");
        scanf("%s", command);
    }

    if (strcmp(command, "sure") == 0) {
        // write_grade_to_file(&grade) ; 
        insertGradeNode(gradeHashTable ,&grade) ;
    }
    else if (strcmp(command, "undo"))
        return;
}

// Add new grade
void addNewGrade() {
    Grade grade;
    // Record user operation
    inputGradeInfo(grade);
    addLog(grade_queue_instance->queue_log, "add a grade information");
}

GradeHashTable *initGradeHashTable() {
    gradeHashTable = getGradeHashTableSingleton() ;

    FILE *fp = fopen(grade_path ,"rb") ;
    Grade grade ;
    while(fread(&grade ,sizeof(Grade) ,1 ,fp)) {
        if(grade.studentId!=0) insertGradeNode(gradeHashTable ,&grade) ;
    }
    return gradeHashTable ;
}

// Delete grade
void deleteGrade(GradeHashTable gradeHashTable ,const char *studentName , int studentId, char *coursename) {
    // Check if the grade exists
    if (!findInGradeHashTable(&gradeHashTable, studentName)) {
        printf("Error: Grade for student '%s' not found.\n", studentName);
        return;
    }

    // Implement the logic to delete a grade based on studentName
    int hashCode = hashFunction(studentName);
    int index = hashCode % gradeHashTable.size;

    GradeNode *prevNode = NULL;
    GradeNode *currentNode = gradeHashTable.data[index].next;

    while (currentNode) {
        if (strcmp(currentNode->grade.studentName, studentName) == 0 &&
            currentNode->grade.studentId == studentId &&
            strcmp(currentNode->grade.coursename , coursename) == 0) {
            // Found the grade, update the linked list
            if (prevNode) {
                prevNode->next = currentNode->next;
            } else {
                gradeHashTable.data[index].next = currentNode->next;
            }

            // Free the memory of the deleted node
            clearGradeNode(currentNode);

            // Update the count
            gradeHashTable.count--;

            // Print a success message
            printf("Grade information for student '%s' deleted successfully.\n", studentName);
            addLog(grade_queue_instance->queue_log, "delete a grade information");
            return;
        }

        prevNode = currentNode;
        currentNode = currentNode->next;
    }

    // Grade not found
    printf("Error: Grade for student '%s' not found.\n", studentName);
}

int findGradeIndex(char *stu_name ,int size) {
    int hashCode = hashFunction(stu_name);
    return hashCode % size;
}

int searchGrade(GradeHashTable gradeHashTable ,char *studentName, int studentId, char *coursename) {
    // Check if the grade exists
    if (!findInGradeHashTable(&gradeHashTable, studentName)) {
        printf("Error: Grade for student '%s' not found.\n", studentName);
        return 0;
    }

    // Implement the logic to search for a grade based on studentName, studentId, and courseId
    int index = findGradeIndex(studentName , gradeHashTable.size) ;
    GradeNode *currentNode = gradeHashTable.data[index].next;

    while (currentNode) {
        if (strcmp(currentNode->grade.studentName, studentName) == 0 &&
            currentNode->grade.studentId == studentId &&
            strcmp(currentNode->grade.coursename , coursename) == 0) {
            // Found the grade, print the information
            printf("Grade information for student '%s', Student ID '%d', CourseName '%s':\n",
                   studentName, studentId, coursename);
            showGradeInfo(currentNode->grade);
            addLog(grade_queue_instance->queue_log, "search a grade information");
            return 1;
        }

        currentNode = currentNode->next;
    }

    // Grade not found
    printf("Error: Grade for student '%s', Student ID '%d', CourseName '%s' not found.\n",
           studentName, studentId, coursename);
    return 1;
}

void showGradeHashTableData(GradeHashTable *gradeHashTable) {
    showAllGradesInHashTable(gradeHashTable) ;
}

void changeGradeInfo(GradeHashTable gradeHashTable ,const char *studentName, int studentId, char *coursename) {
    // Check if the grade exists
    if (!findInGradeHashTable(&gradeHashTable, studentName)) {
        printf("Error: Grade for student '%s' not found.\n", studentName);
        return;
    }

    // Implement the logic to change grade information based on studentName, studentId, and courseId
    int hashCode = hashFunction(studentName);
    int index = hashCode % gradeHashTable.size;

    GradeNode *currentNode = gradeHashTable.data[index].next;

    while (currentNode) {
        if (strcmp(currentNode->grade.studentName, studentName) == 0 &&
            currentNode->grade.studentId == studentId &&
            strcmp(currentNode->grade.coursename , coursename) == 0) {
            // Found the grade, modify the information
            printf("Enter new grade information for student '%s', Student ID '%d', CourseName '%s':\n",
                   studentName, studentId, coursename);
            printf("Score : ") ;
            scanf("%f" ,&currentNode->grade.score) ;
            // Print a success message
            printf("Grade information for student '%s', Student ID '%d', CourseName '%s' updated successfully.\n",
                   studentName, studentId, coursename);
            addLog(grade_queue_instance->queue_log, "change a grade information");
            return;
        }

        currentNode = currentNode->next;
    }

    // Grade not found
    printf("Error: Grade for student '%s', Student ID '%d', CourseName '%d' not found.\n",
           studentName, studentId, coursename);
}

typedef struct class_max_min {
    float max ;
    float min ;
    float avg ;
    int count ;
} class_max_min ;
//计算班级最高分，最低分，平均分

class_max_min *class_num = NULL ;

typedef struct school_max_min {
    float max ;
    float min ;
    float avg ;
    int count ;
} school_max_min ;

school_max_min *school_num = NULL ;

void initSchoolAndClassNum() {

    // 为 school_max_min 分配内存
    school_num = (school_max_min *)malloc(sizeof(school_max_min));

    // 检查分配是否成功
    if (school_num == NULL) {
        // 处理分配失败
        printf("内存分配失败。\n");
        exit(1);
    }

    // 初始化值
    school_num->max = 999;
    school_num->min = 999;
    school_num->avg = 0.0;

    // 将计数初始化为零
    class_num->count = 0;
    // 为 school_max_min 分配内存
    class_num = (class_max_min *)malloc(sizeof(class_max_min));

    // 检查分配是否成功
    if (school_num == NULL) {
        // 处理分配失败
        printf("内存分配失败。\n");
        exit(1);
    }

    // 初始化值
    class_num->max = 999;
    class_num->min = 999;
    class_num->avg = 0.0;

    // 将计数初始化为零
    class_num->count = 0;
}

GradeNode *findGradeByclass(student_avl_node *root ,int gradeId ,int classId) {
    int index = findGradeIndex(root->key.name ,gradeHashTable->size) ;
    GradeNode *currentNode = gradeHashTable->data[index].next;

    while (currentNode) {
        if (gradeId == root->key.grade && classId == root->key.classId) break;

        currentNode = currentNode->next;
    }

    return currentNode ;
}

GradeNode *findGradeByschool(student_avl_node *root , int gradeId) {
    int index = findGradeIndex(root->key.name ,gradeHashTable->size) ;
    GradeNode *currentNode = gradeHashTable->data[index].next;

    while (currentNode) {
        if (gradeId == root->key.grade) break;
        currentNode = currentNode->next;
    }

    return currentNode ;
}

void alo_class_grade(Grade grade) {
    // 最高分
    if (class_num->max < grade.score) class_num->max = grade.score;

    // 最低分
    if (class_num->min > grade.score) class_num->min = grade.score;

    // 平均分
    class_num->avg += grade.score; // 更正

    // 还应该跟踪成绩数量以便稍后计算平均值
    class_num->count++; // 添加这一行
}

//计算班级最高分，最低分，总分
void gradeInClass(student_avl_node *root ,int gradeId ,int classId) {
    if(root == STU_NIL) return ;
    gradeInClass(root->lchild ,gradeId ,classId) ; 
    alo_class_grade(findGradeByclass(root ,gradeId ,classId)->grade) ;
    gradeInClass(root->rchild ,gradeId ,classId) ;
}

// 类似地，在 alo_school_grade 函数中，更正更新平均分的条件
void alo_school_grade(Grade grade) {
    // 最高分
    if (school_num->max < grade.score) school_num->max = grade.score;

    // 最低分
    if (school_num->min > grade.score) school_num->min = grade.score;

    // 平均分
    school_num->avg += grade.score; // 更正

    // 还应该跟踪成绩数量以便稍后计算平均值
    school_num->count++; // 添加这一行
}

void gradeInSchool(student_avl_node *root ,int gradeId) {
    if(root == STU_NIL) return ;
    gradeInSchool(root->lchild ,gradeId) ; 
    alo_school_grade(findGradeByschool(root ,gradeId)->grade) ;
    gradeInSchool(root->rchild ,gradeId) ;
}

// 显示班级的最大分、最小分、平均分
void displayClassStats(student_avl_node *root ,int gradeId ,int classId) {
    gradeInClass(root ,gradeId ,classId) ;
    if (class_num->count == 0) {
        printf("No grades in the class.\n");
        return;
    }

    printf("Class Statistics:\n");
    printf("Maximum Score: %.2f\n", class_num->max);
    printf("Minimum Score: %.2f\n", class_num->min);
    printf("Average Score: %.2f\n", class_num->avg / class_num->count);
}

// 显示全校的最大分、最小分、平均分
void displaySchoolStats(student_avl_node *root ,int gradeId) {
    gradeInSchool(root ,gradeId) ;
    if (school_num->count == 0) {
        printf("No grades in the school.\n");
        return;
    }

    printf("School Statistics:\n");
    printf("Maximum Score: %.2f\n", school_num->max);
    printf("Minimum Score: %.2f\n", school_num->min);
    printf("Average Score: %.2f\n", school_num->avg / school_num->count);
}

// 查找一个人的所有成绩
GradeNode* findGradesForPerson(const char* studentName) {
    
    // Check if the person exists in the hash table
    if (!findInGradeHashTable(gradeHashTable, studentName)) {
        printf("Error: Person '%s' not found.\n", studentName);
        return NULL;
    }

    // Implement the logic to search for grades based on studentName
    int index = hashFunction(studentName) % gradeHashTable->size;
    GradeNode* personGrades = NULL;
    GradeNode* currentNode = gradeHashTable->data[index].next;

    while (currentNode) {
        if (strcmp(currentNode->grade.studentName, studentName) == 0) {
            // Found a grade for the person, add it to the list
            GradeNode* newGradeNode = getNewGradeNode(&(currentNode->grade));
            newGradeNode->next = personGrades;
            personGrades = newGradeNode;
        }
        currentNode = currentNode->next;
    }

    return personGrades;
}

// 打印一个人的所有成绩
void printPersonGrades(GradeNode* personGrades) {
    printf("Person's Grades:\n");

    while (personGrades != NULL) {
        printf("Course: %s, Score: %.2f\n", personGrades->grade.coursename, personGrades->grade.score);
        personGrades = personGrades->next;
    }
}

// 释放链表内存
void freeGradeList(GradeNode* head) {
    while (head != NULL) {
        GradeNode* temp = head;
        head = head->next;
        clearGradeNode(temp);
    }
}

// 冒泡排序
void bubbleSort(GradeNode* grades) {
    int swapped;
    GradeNode* ptr1;
    GradeNode* lptr = NULL;

    // Check for empty list
    if (grades == NULL)
        return;

    do {
        swapped = 0;
        ptr1 = grades;

        while (ptr1->next != lptr) {
            if (ptr1->grade.score < ptr1->next->grade.score) {
                // 交换节点内容
                Grade temp = ptr1->grade;
                ptr1->grade = ptr1->next->grade;
                ptr1->next->grade = temp;

                swapped = 1;
            }
            ptr1 = ptr1->next;
        }
        lptr = ptr1;
    } while (swapped);
}


// 从大到小展示学生成绩
void displayGradesDescending() {
    GradeNode *grades = getAllGradesInHashTable(gradeHashTable) ;

    if (grades == NULL) {
        printf("No grades to display.\n");
        return;
    }

    // 先进行排序
    bubbleSort(grades);

    // 打印排序后的成绩
    printf("+-----------------------+----------------+------------------+-------+\n");
    printf("|    Student Name       |  Student ID    |   Course Name    | Score |\n");
    printf("+-----------------------+----------------+------------------+-------+\n");

    GradeNode* currentNode = grades;
    while (currentNode != NULL) {
        printf("| %-21s | %-14d | %-16s | %.2f  |\n",
               currentNode->grade.studentName, currentNode->grade.studentId,
               currentNode->grade.coursename, currentNode->grade.score);
        currentNode = currentNode->next;
    }

    printf("+-----------------------+----------------+------------------+-------+\n");
}

#endif
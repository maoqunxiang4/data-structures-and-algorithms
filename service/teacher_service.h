#ifndef Teacher_Service_H
#define Teacher_Service_H
#include "../utils/read_basic_info.h"
#include "../utils/write_basic_info.h"
#include "../basic_data_structuers/data-structers/teacher_avl.h"
#include "../utils/constants.h"
#include "../model/log_model.h"

// enum teacher_data {
//     id,
//     age,
//     name,
//     gender,
//     hireDate,
//     subject,
//     classId
// };

queue_singleton *teacher_queue_instance = NULL;
teacher_avl_node *teacher_root = TEA_NIL ;

void setTeacherQueueinstance() {
    teacher_queue_instance = getQueueSingleton();
}

teacher_avl_node *inputTeacherInfo(Teacher teacher) {
    printf("Enter teacher information:\n");

    printf("Teacher ID: ");
    scanf("%d", &teacher.id);

    printf("Age: ");
    scanf("%d", &teacher.age);

    printf("Name: ");
    scanf("%s", teacher.name);

    printf("Gender: ");
    scanf("%s", teacher.gender);

    printf("Hire Date: ");
    scanf("%s", teacher.hireDate);

    printf("Subject: ");
    scanf("%s", teacher.subject);

    printf("Class ID: ");
    scanf("%d", &teacher.classId);

    printf("make sure the information of the teacher is right.");
    showTeacherInfo(teacher);
    printf("if you want to undo, input \"undo\" else you can input \"sure\":");

    char command[10];
    scanf("%s", command);
    while (strcmp(command, "sure") != 0 && strcmp(command, "undo") != 0) {
        printf("please input again...");
        scanf("%s", command);
    }

    if (strcmp(command, "sure") == 0) {
        // write_teacher_to_file(&teacher);
        teacher_root = teacher_avl_insert(teacher_root ,teacher) ;
        return teacher_root ;
    }


    else if (strcmp(command, "undo"))
        return teacher_root;
}

teacher_avl_node *initTeacherInfo() {
    teacher_root = getTeacherSingleton();

    FILE *fp = fopen(teacher_path, "rb");
    Teacher teacher;
    while (fread(&teacher, sizeof(Teacher), 1, fp)) {
        if (teacher.id != 0)
            teacher_root = teacher_avl_insert(teacher_root, teacher);
    }

    return teacher_root;
}

teacher_avl_node *addNewTeacher() {
    Teacher teacher;
    teacher_root = inputTeacherInfo(teacher);
    addLog(teacher_queue_instance->queue_log, "add a teacher information");
    return teacher_root ;
}

teacher_avl_node *deleteTeacher(int id) {
    FILE *fp = fopen(teacher_path, "r+b");
    if (fp == NULL) {
        printf("the file is not exist here");
    }

    int offset = 0;
    Teacher teacher;
    while (fread(&teacher, sizeof(Teacher), 1, fp) == 1) {
        if (teacher.id == id)
            break;
        offset++;
    }
    fseek(fp, sizeof(Teacher) * offset, SEEK_SET);

    Teacher emptyTeacher = {0};
    fwrite(&emptyTeacher, sizeof(Teacher), 1, fp);
    fclose(fp);
    teacher_root = teacher_avl_erase(teacher_root ,id) ;
    addLog(teacher_queue_instance->queue_log, "delete a teacher information");
    return teacher_root ;
}

Teacher searchTeacher(int teacher_id) {
    addLog(teacher_queue_instance->queue_log, "search a teacher information");
    return teacher_avl_search(teacher_root, teacher_id);
}

teacher_avl_node *changeTeacherInfo(int id) {
    FILE *fp = fopen(teacher_path, "r+b");
    if (fp == NULL) {
        printf("the file is not exist here");
    }

    int offset = 0;
    Teacher teacher;
    while (fread(&teacher, sizeof(Teacher), 1, fp) == 1) {
        if (teacher.id == id)
            break;
        offset++;
    }
    fseek(fp, sizeof(Teacher) * offset, SEEK_SET);

    printf("Enter teacher information:\n");

    printf("Teacher ID: ");
    scanf("%d", &teacher.id);

    printf("Age: ");
    scanf("%d", &teacher.age);

    printf("Name: ");
    scanf("%s", teacher.name);

    printf("Gender: ");
    scanf("%s", teacher.gender);

    printf("Hire Date: ");
    scanf("%s", teacher.hireDate);

    printf("Subject: ");
    scanf("%s", teacher.subject);

    printf("Class ID: ");
    scanf("%d", &teacher.classId);

    printf("make sure the information of the teacher is right.");
    showTeacherInfo(teacher);
    printf("if you want to undo, input \"undo\" else you can input \"sure\":");
    
    char command[10];
    scanf("%s" ,command) ;
    while(strcmp(command ,"sure")!=0&&strcmp(command ,"undo")!=0) {
        printf("please input again...") ;
        scanf("%s" ,command) ;
    }

    if(strcmp(command ,"sure")==0) {
        fwrite(&teacher, sizeof(Teacher), 1, fp);
        fclose(fp);
        teacher_root = teacher_avl_erase(teacher_root,id) ;
        teacher_root = teacher_avl_insert(teacher_root,teacher) ;
    }

    addLog(teacher_queue_instance->queue_log, "change a teacher information");
    return teacher_root ;
}

void showAllTeachersInfo() {
    if (teacher_root == TEA_NIL)
        return;

    printf("+------+-----+-------------------------+--------+-------------------+-------------------------+----------+\n");
    printf("|  ID  | Age |          Name           | Gender |     Hire Date     |         Subject         | Class ID |\n");
    printf("+------+-----+-------------------------+--------+-------------------+-------------------------+----------+\n");

    teacher_inOrder(teacher_root);

    printf("+------+-----+-------------------------+--------+-------------------+-------------------------+----------+\n\n\n\n");
}

#endif
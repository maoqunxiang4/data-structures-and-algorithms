#ifndef Log_Queue_H
#define Log_Queue_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"
#include "../../utils/constants.h"

// 定义日志结构体
typedef struct log_node {
    char *name;
    struct tm *time;
    char *command;
} log_node;

// 定义队列结构体
typedef struct queue_log {
    log_node *logArr;
    int front;
    int rear;
    int cnt;
    int size;
} queue_log;

typedef struct queue_singleton {
    queue_log *queue_log ;
} queue_singleton ;

log_node *getLogNode(const char *name, const char *command) {
    log_node *logEntry = (log_node *)malloc(sizeof(log_node));
    if (!logEntry) {
        // 处理内存分配失败的情况
        return NULL;
    }

    // 为字符串分配内存，并将内容复制到新分配的内存中
    logEntry->name = strdup(name);
    logEntry->command = strdup(command);
    time_t currentTime ;
    time(&currentTime) ;
    logEntry->time = gmtime(&currentTime);

    return logEntry;
}

// 初始化队列
queue_log *log_initQueue(int size) {
    queue_log *q = (queue_log *)malloc(sizeof(queue_log));
    q->logArr = (log_node *)malloc(sizeof(log_node) * size);
    q->front = 0;
    q->rear = -1;
    q->cnt = 0;
    q->size = size;
    return q;
}

queue_singleton *getQueueSingleton() {
    //局部静态变量存在于程序运行的整体过程，但是只在函数内部可见，不过可以通过指针在外部进行操作
    static queue_singleton queue_singleton ;
    queue_singleton.queue_log = log_initQueue(50) ;
    return &queue_singleton ;
}

// 判断队列是否为空
int log_isEmpty(queue_log *q) {
    return q->cnt == 0;
}

// 判断队列是否已满
int log_isFull(queue_log *q) {
    return q->cnt == q->size;
}

// 入队
void log_enqueue(queue_log *q, log_node *data) {
    if (!log_isFull(q)) {
        q->rear = (q->rear + 1) % q->size;
        q->logArr[q->rear] = *data;
        q->cnt++;
    } else {
        printf("Queue is full. Cannot enqueue.\n");
    }
}

// 出队
log_node log_dequeue(queue_log *q) {
    log_node emptyLog;
    emptyLog.name = NULL;
    emptyLog.time = NULL;
    emptyLog.command = NULL;

    if (!log_isEmpty(q)) {
        log_node data = q->logArr[q->front];
        q->front = (q->front + 1) % q->size;
        q->cnt--;
        return data;
    } else {
        printf("Queue is empty. Cannot dequeue.\n");
        return emptyLog;
    }
}

void freeLogNode(log_node *logEntry) {
    if (logEntry) {
        // 释放字符串内存
        free(logEntry->name);
        free(logEntry->command);

        // 释放时间内存
        free(logEntry->time);
    }
}

// 释放队列内存
void log_freeQueue(queue_log *q) {
    for(int i = 0 ; i < q->cnt ; i ++) {
        freeLogNode(&q->logArr[i]) ;
    }
    free(q->logArr);
    free(q);
}

#endif
#ifndef Course_AVL_H
#define Course_AVL_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "../../utils/constants.h"
#include "../../user_data_structers/user_data_structers.h"

typedef struct course_avl_node {
    Course key; // Modify this
    int height;
    struct course_avl_node *lchild, *rchild;
} course_avl_node;

course_avl_node cou__NIL;
#define COU_NIL (&cou__NIL)

void initCourseNIL() {
    COU_NIL->key.courseId = -1; // Modify this
    COU_NIL->height = 0;
    COU_NIL->lchild = COU_NIL;
    COU_NIL->rchild = COU_NIL;
}

#define H(n) (n->height)
#define R(n) (n->rchild)
#define L(n) (n->lchild)

typedef struct course_root {
    course_avl_node *root ;
} course_root;

course_avl_node *getCourseSingleton() { 
    static course_root course_root ;
    course_root.root = COU_NIL;
    return course_root.root;
}

course_avl_node *course_avl_getNewNode(Course key) { // Modify this
    course_avl_node *node = (course_avl_node *)malloc(sizeof(course_avl_node));
    node->key = key;
    node->height = 1;
    node->lchild = COU_NIL;
    node->rchild = COU_NIL;
    return node;
}

course_avl_node *course_predecessor(course_avl_node *root) {
    course_avl_node *p = root;
    while (p != COU_NIL) {
        p = p->rchild;
    }
    return p;
}

void course_swap(char *str1, char *str2) {
    char *temp = (char *)malloc(sizeof(char) * 100);
    temp = strdup(str1);
    str1 = strdup(str2);
    str2 = strdup(temp);
}

void course_updateHeight(course_avl_node *root) {
    root->height = (root->lchild->height > root->rchild->height ? root->lchild->height : root->rchild->height) + 1;
}

course_avl_node *course_left_rotate(course_avl_node *root) {
    course_avl_node *p = root->rchild;
    root->rchild = p->lchild;
    p->lchild = root;
    course_updateHeight(root);
    course_updateHeight(p);
    return p;
}

course_avl_node *course_right_rotate(course_avl_node *root) {
    course_avl_node *p = root->lchild;
    root->lchild = p->rchild;
    p->rchild = root;
    course_updateHeight(root);
    course_updateHeight(p);
    return p;
}

course_avl_node *course_maintain(course_avl_node *root) {

    if (abs(root->rchild->height - root->lchild->height) <= 1)
        return root;
    else {
        if (root->lchild->height > root->rchild->height) {
            if (root->lchild->rchild->height > root->lchild->lchild->height )
            {
                root->lchild = course_left_rotate(root->lchild);
            }
            root = course_right_rotate(root);
        }

        if (root->rchild->height > root->lchild->height)
        {
            if (root->rchild->lchild->height > root->rchild->rchild->height)
            {
                root->rchild = course_right_rotate(root->rchild);
            }
            root = course_left_rotate(root);
        }
    }
    return root;
}

course_avl_node *course_avl_insert(course_avl_node *root, Course key)
{
    if (root == COU_NIL)
        return course_avl_getNewNode(key);
    else if (root->key.courseId == key.courseId)
        return root;
    else if (root->key.courseId > key.courseId)
        root->lchild = course_avl_insert(root->lchild, key);
    else if (root->key.courseId < key.courseId)
        root->rchild = course_avl_insert(root->rchild, key);
    course_updateHeight(root);
    return course_maintain(root);
}

course_avl_node *course_avl_erase(course_avl_node *root, int courseId)
{
    if (root == COU_NIL)
        return root;
    if (root->key.courseId > courseId)
        root->lchild = course_avl_erase(root->lchild, courseId);
    else if (root->key.courseId < courseId)
        root->rchild = course_avl_erase(root->rchild, courseId);
    else
    {
        if (root->lchild == COU_NIL || root->rchild == COU_NIL)
        {
            course_avl_node *p = root->lchild != COU_NIL ? root->lchild : root->rchild;
            free(root);
            return p;
        }
        else
        {
            course_avl_node *p = course_predecessor(root->lchild);
            root->key = p->key;
            root->lchild = course_avl_erase(root->lchild, courseId);
        }
    }
    course_updateHeight(root);   
    return course_maintain(root);
}

Course course_avl_search(course_avl_node *root, int courseId)
{
    if (root == COU_NIL || root->key.courseId == courseId)
        return root->key;
    if (root->key.courseId > courseId)
        return course_avl_search(root->lchild, courseId);
    else
        return course_avl_search(root->rchild, courseId);
}

void course_clearAVL(course_avl_node *root)
{
    if (root == COU_NIL)
        return;
    course_clearAVL(root->lchild);
    course_clearAVL(root->rchild);
    free(root) ;
}

void course_inOrder(course_avl_node *root)
{
    if (root == COU_NIL)
        return;

    course_inOrder(root->lchild);

    printf("| %-9d | %-23s | %-6d | %-25s |\n",
           root->key.courseId, root->key.courseName, root->key.credit, root->key.teacherName);

    course_inOrder(root->rchild);
}

#endif

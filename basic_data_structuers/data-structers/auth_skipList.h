#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../user_data_structers/user_data_structers.h"

// Skip list node structure
typedef struct SkipNode {
    auth data;
    struct SkipNode** forward;
} SkipNode;

// Skip list structure
typedef struct {
    int max_level;
    int level;
    SkipNode* header;
} SkipList;

// Function to create a new node
SkipNode* skip_createNode(int level, auth data) {
    SkipNode* newNode = (SkipNode*)malloc(sizeof(SkipNode));
    if (newNode == NULL) {
        // Handle memory allocation failure
        exit(EXIT_FAILURE);
    }

    // Deep copy role and action to avoid potential issues
    strncpy(newNode->data.role, data.role, sizeof(newNode->data.role) - 1);
    newNode->data.role[sizeof(newNode->data.role) - 1] = '\0';

    strncpy(newNode->data.action, data.action, sizeof(newNode->data.action) - 1);
    newNode->data.action[sizeof(newNode->data.action) - 1] = '\0';

    newNode->forward = (SkipNode**)malloc(sizeof(SkipNode*) * (level + 1));
    if (newNode->forward == NULL) {
        // Handle memory allocation failure
        free(newNode);
        exit(EXIT_FAILURE);
    }

    memset(newNode->forward, 0, sizeof(SkipNode*) * (level + 1));
    return newNode;
}

// Function to generate a random level for a new node
int randomLevel() {
    int level = 0;
    while (rand() % 2 && level < 16) {
        level++;
    }
    return level;
}

// Function to create a new skip list
SkipList* createSkipList() {
    SkipList* skipList = (SkipList*)malloc(sizeof(SkipList));
    if (skipList == NULL) {
        // Handle memory allocation failure
        exit(EXIT_FAILURE);
    }

    skipList->max_level = 16; // Maximum level for the skip list
    skipList->level = 0;
    skipList->header = skip_createNode(skipList->max_level, (auth){"", ""});
    return skipList;
}

// Function to insert a node into the skip list
void auth_insert(SkipList* skipList, auth data) {
    SkipNode* update[skipList->max_level + 1];
    memset(update, 0, sizeof(SkipNode*) * (skipList->max_level + 1));

    SkipNode* current = skipList->header;

    for (int i = skipList->level; i >= 0; i--) {
        while (current->forward[i] != NULL && strcmp(current->forward[i]->data.role, data.role) < 0) {
            current = current->forward[i];
        }
        update[i] = current;
    }

    int newLevel = randomLevel();

    if (newLevel > skipList->level) {
        for (int i = skipList->level + 1; i <= newLevel; i++) {
            update[i] = skipList->header;
        }
        skipList->level = newLevel;
    }

    SkipNode* newNode = skip_createNode(newLevel, data);

    for (int i = 0; i <= newLevel; i++) {
        newNode->forward[i] = update[i]->forward[i];
        update[i]->forward[i] = newNode;
    }
}

// Function to search for a node in the skip list
SkipNode* auth_search(SkipList* skipList, char* role) {
    SkipNode* current = skipList->header;
    for (int i = skipList->level; i >= 0; i--) {
        while (current->forward[i] != NULL && strcmp(current->forward[i]->data.role, role) < 0) {
            current = current->forward[i];
        }
    }
    current = current->forward[0];

    if (current != NULL && strcmp(current->data.role, role) == 0) {
        return current;
    } else {
        return NULL;
    }
}

// Function to print the skip list
void printSkipList(SkipList* skipList) {
    printf("Skip List:\n");
    for (int i = skipList->level; i >= 0; i--) {
        SkipNode* current = skipList->header->forward[i];
        printf("Level %d: ", i);
        while (current != NULL) {
            printf("(%s, %s) ", current->data.role, current->data.action);
            current = current->forward[i];
        }
        printf("\n");
    }
}

// Function to free the memory allocated for the skip list
void freeSkipList(SkipList* skipList) {
    SkipNode* current = skipList->header->forward[0];
    SkipNode* temp;

    while (current != NULL) {
        temp = current;
        current = current->forward[0];
        free(temp->forward);
        free(temp);
    }

    free(skipList->header->forward);
    free(skipList->header);
    free(skipList);
}


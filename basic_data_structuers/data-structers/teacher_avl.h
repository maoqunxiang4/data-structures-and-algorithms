#ifndef Teacher_AVL_H
#define Teacher_AVL_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "../../utils/constants.h"
#include "../../user_data_structers/user_data_structers.h"

typedef struct teacher_avl_node {
    Teacher key; 
    int height;
    struct teacher_avl_node *lchild, *rchild;
} teacher_avl_node;

teacher_avl_node tea__NIL;
#define TEA_NIL (&tea__NIL)
void initNIL() {
    TEA_NIL->key.id = -1; 
    TEA_NIL->height = 0;
    TEA_NIL->lchild = TEA_NIL;
    TEA_NIL->rchild = TEA_NIL;
}

#define H(n) (n->height)
#define R(n) (n->rchild)
#define L(n) (n->lchild)

typedef struct teacher_tree {
    teacher_avl_node *root ;
}teacher_tree ;

teacher_avl_node *getTeacherSingleton() {
    static teacher_tree teacher_tree  ;
    teacher_tree.root = TEA_NIL ;
    return teacher_tree.root;
}

teacher_avl_node *teacher_avl_getNewNode(Teacher key) {
    teacher_avl_node *node = (teacher_avl_node *)malloc(sizeof(teacher_avl_node));
    node->key = key;
    node->height = 1;
    node->lchild = TEA_NIL;
    node->rchild = TEA_NIL;
    return node;
}

teacher_avl_node *teacher_predecessor(teacher_avl_node *root) {
    teacher_avl_node *p = root;
    while (p != TEA_NIL) {
        p = p->rchild;
    }
    return p;
}

void teacher_swap(char *str1, char *str2) {
    char *temp = (char *)malloc(sizeof(char) * 100);
    temp = strdup(str1);
    str1 = strdup(str2);
    str2 = strdup(temp);
}

void teacher_updateHeight(teacher_avl_node *root) {
    //这里写的有问题，其实应该，我对三目表达式的掌握不够
    // H(root) = H(R(root)) >= H(L(root)) ? H(R(root)) + 1 : H(L(root)) + 1;
    root->height = (root->lchild->height > root->rchild->height ? root->lchild->height : root->rchild->height) + 1;
}

teacher_avl_node *teacher_left_rotate(teacher_avl_node *root) {
    teacher_avl_node *p = root->rchild;
    root->rchild = p->lchild;
    p->lchild = root;
    teacher_updateHeight(root);
    teacher_updateHeight(p);
    return p;
}

teacher_avl_node *teacher_right_rotate(teacher_avl_node *root) {
    teacher_avl_node *p = root->lchild;
    root->lchild = p->rchild;
    p->rchild = root;
    teacher_updateHeight(root);
    teacher_updateHeight(p);
    return p;
}

teacher_avl_node *teacher_maintain(teacher_avl_node *root) {

    if (abs(root->rchild->height - root->lchild->height) <= 1)
        return root;
    else {
        //冲突发生了
        //L子树
        if (root->lchild->height > root->rchild->height) {
            //LR型
            if (root->lchild->rchild->height > root->lchild->lchild->height )
            {
                root->lchild = teacher_left_rotate(root->lchild);
            }
            root = teacher_right_rotate(root);
        }

        //R子树
        if (root->rchild->height > root->lchild->height)
        {
            //RL型
            if (root->rchild->lchild->height > root->rchild->rchild->height)
            {
                root->rchild = teacher_right_rotate(root->rchild);
            }
            root = teacher_left_rotate(root);
        }
    }
    return root;
}

teacher_avl_node *teacher_avl_insert(teacher_avl_node *root, Teacher key)
{
    if (root == TEA_NIL)
        return teacher_avl_getNewNode(key);
    else if (root->key.id == key.id)
        return root;
    else if (root->key.id > key.id)
        root->lchild = teacher_avl_insert(root->lchild, key);
    else if (root->key.id < key.id)
        root->rchild = teacher_avl_insert(root->rchild, key);
    teacher_updateHeight(root);
    return teacher_maintain(root);
}

teacher_avl_node *teacher_avl_erase(teacher_avl_node *root, int id)
{
    if (root == TEA_NIL)
        return root;
    if (root->key.id > id)
        root->lchild = teacher_avl_erase(root->lchild, id);
    else if (root->key.id < id)
        root->rchild = teacher_avl_erase(root->rchild, id);
    else
    {
        //如果是出度为0或者出度为1的点：出度为0直接删除；出度为1，子节点直接接入祖父节点中
        if (root->lchild == TEA_NIL || root->rchild == TEA_NIL)
        {
            teacher_avl_node *p = root->lchild != TEA_NIL ? root->lchild : root->rchild;
            free(root);
            //直接接入到祖父节点中，在祖父节点中平衡
            return p;
        }
        //如果是出度为2的点：转化为出度为1的节点
        else
        {
            teacher_avl_node *p = teacher_predecessor(root->lchild);
            root->key = p->key;
            root->lchild = teacher_avl_erase(root->lchild, id);
        }
    }
    teacher_updateHeight(root);   
    return teacher_maintain(root);
}

Teacher teacher_avl_search(teacher_avl_node *root, int id)
{
    if (root == TEA_NIL || root->key.id == id)
        return root->key;
    if (root->key.id > id)
        return teacher_avl_search(root->lchild, id);
    else
        return teacher_avl_search(root->rchild, id);
}

void teacher_clearAVL(teacher_avl_node *root)
{
    if (root == TEA_NIL)
        return;
    teacher_clearAVL(root->lchild);
    teacher_clearAVL(root->rchild);
    free(root) ;
}

void showTeacherInfo(Teacher teacher) {
    printf("\n------------------------teacher information-------------------------------\n");
    printf("ID: %d\n", teacher.id);
    printf("Age: %d\n", teacher.age);
    printf("Name: %s\n", teacher.name);
    printf("Gender: %s\n", teacher.gender);
    printf("Hire Date: %s\n", teacher.hireDate);
    printf("Subject: %s\n", teacher.subject);
    printf("Class ID: %d\n", teacher.classId);
    printf("\n------------------------teacher information-------------------------------\n");
}

void teacher_inOrder(teacher_avl_node *root)
{
    if (root == TEA_NIL)
        return;
    
    teacher_inOrder(root->lchild);

    printf("| %-4d | %-3d | %-23s | %-6s | %-17s | %-25s | %-8d |\n",
           root->key.id, root->key.age, root->key.name,
           root->key.gender, root->key.hireDate, root->key.subject, root->key.classId);

    teacher_inOrder(root->rchild);
}

#endif

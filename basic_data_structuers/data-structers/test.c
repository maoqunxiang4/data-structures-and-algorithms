#include <stdio.h>
#include <stdlib.h>
#include "Grade_Hash.h"

int main() {
    // Creating an empty Grade hash table
    GradeHashTable *hashTable = getNewGradeHashTable(10);

    // Adding some sample grades to the hash table
    Grade grade1 = {"John", 1, 101, 85.5};
    Grade grade2 = {"Alice", 2, 102, 92.0};
    Grade grade3 = {"Bob", 3, 103, 78.3};
    Grade grade4 = {"Mary", 4, 104, 90.2};
    Grade grade5 = {"David", 5, 105, 88.7};

    insertGradeNode(hashTable, &grade1);
    insertGradeNode(hashTable, &grade2);
    insertGradeNode(hashTable, &grade3);
    insertGradeNode(hashTable, &grade4);
    insertGradeNode(hashTable, &grade5);

    // Testing findInGradeHashTable
    printf("Searching for grades:\n");
    printf("John's grade: %s\n", findInGradeHashTable(hashTable, "John") ? "Found" : "Not Found");
    printf("Bob's grade: %s\n", findInGradeHashTable(hashTable, "Bob") ? "Found" : "Not Found");
    printf("Eva's grade: %s\n", findInGradeHashTable(hashTable, "Eva") ? "Found" : "Not Found");

    // Clearing the Grade hash table to free memory
    clearGradeHashTable(hashTable);

    return 0;
}

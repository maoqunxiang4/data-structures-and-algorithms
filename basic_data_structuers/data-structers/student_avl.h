#ifndef Student_AVL_H
#define Student_AVL_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "../../utils/constants.h"
#include "../../user_data_structers/user_data_structers.h"

typedef struct student_avl_node {
    Student key; // 修改这里
    int height;
    struct student_avl_node *lchild, *rchild;
} student_avl_node;

typedef struct student_tree {
    student_avl_node *root ;
} student_tree ;

student_avl_node *student_avl_getNewNode(Student key) ;

student_avl_node stu__NIL;
#define STU_NIL (&stu__NIL)
void initSTU_NIL() {
    STU_NIL->key.id = -1; // 修改这里
    STU_NIL->height = 0;
    STU_NIL->lchild = STU_NIL;
    STU_NIL->rchild = STU_NIL;
}

#define H(n) (n->height)
#define R(n) (n->rchild)
#define L(n) (n->lchild)

student_tree *getStudentTreeSingleton() {
    static student_tree student_tree  ;
    student_tree.root = STU_NIL ;
    return &student_tree ;
}

student_avl_node *student_avl_getNewNode(Student key) { // 修改这里
    student_avl_node *node = (student_avl_node *)malloc(sizeof(student_avl_node));
    node->key = key;
    node->height = 1;
    node->lchild = STU_NIL;
    node->rchild = STU_NIL;
    return node;
}

student_avl_node *student_avl_predecessor(student_avl_node *root) {
    student_avl_node *p = root;
    while (p != STU_NIL) {
        p = p->rchild;
    }
    return p;
}

void student_swap(char *str1, char *str2) {
    char *temp = (char *)malloc(sizeof(char) * 100);
    temp = strdup(str1);
    str1 = strdup(str2);
    str2 = strdup(temp);
}

void student_avl_updateHeight(student_avl_node *root) {
    //这里写的有问题，其实应该，我对三目表达式的掌握不够
    // H(root) = H(R(root)) >= H(L(root)) ? H(R(root)) + 1 : H(L(root)) + 1;
    root->height = (root->lchild->height > root->rchild->height ? root->lchild->height : root->rchild->height) + 1;
}

student_avl_node *student_avl_left_rotate(student_avl_node *root) {
    student_avl_node *p = root->rchild;
    root->rchild = p->lchild;
    p->lchild = root;
    student_avl_updateHeight(root);
    student_avl_updateHeight(p);
    return p;
}

student_avl_node *student_avl_right_rotate(student_avl_node *root) {
    student_avl_node *p = root->lchild;
    root->lchild = p->rchild;
    p->rchild = root;
    student_avl_updateHeight(root);
    student_avl_updateHeight(p);
    return p;
}

student_avl_node *student_avl_maintain(student_avl_node *root) {

    if (abs(root->rchild->height - root->lchild->height) <= 1)
        return root;
    else {
        //冲突发生了
        //L子树
        if (root->lchild->height > root->rchild->height) {
            //LR型
            if (root->lchild->rchild->height > root->lchild->lchild->height )
            {
                root->lchild = student_avl_left_rotate(root->lchild);
            }
            root = student_avl_right_rotate(root);
        }

        //R子树
        if (root->rchild->height > root->lchild->height)
        {
            //RL型
            if (root->rchild->lchild->height > root->rchild->rchild->height)
            {
                root->rchild = student_avl_right_rotate(root->rchild);
            }
            root = student_avl_left_rotate(root);
        }
    }
    return root;
}

student_avl_node *student_avl_insert(student_avl_node *root, Student key)
{
    if (root == STU_NIL)
        return student_avl_getNewNode(key);
    else if (root->key.id == key.id)
        return root;
    else if (root->key.id > key.id)
        root->lchild = student_avl_insert(root->lchild, key);
    else if (root->key.id < key.id)
        root->rchild = student_avl_insert(root->rchild, key);
    student_avl_updateHeight(root);
    return student_avl_maintain(root);
}

student_avl_node *student_avl_erase(student_avl_node *root, int id)
{
    if (root == STU_NIL)
        return root;
    if (root->key.id > id)
        root->lchild = student_avl_erase(root->lchild, id);
    else if (root->key.id < id)
        root->rchild = student_avl_erase(root->rchild, id);
    else
    {
        //如果是出度为0或者出度为1的点：出度为0直接删除；出度为1，子节点直接接入祖父节点中
        if (root->lchild == STU_NIL || root->rchild == STU_NIL)
        {
            student_avl_node *p = root->lchild != STU_NIL ? root->lchild : root->rchild;
            free(root);
            //直接接入到祖父节点中，在祖父节点中平衡
            return p;
        }
        //如果是出度为2的点：转化为出度为1的节点
        else
        {
            student_avl_node *p = student_avl_predecessor(root->lchild);
            root->key = p->key;
            root->lchild = student_avl_erase(root->lchild, id);
        }
    }
    student_avl_updateHeight(root);   
    return student_avl_maintain(root);
}

Student student_avl_search(student_avl_node *root, int id)
{
    if (root == STU_NIL || root->key.id == id)
        return root->key;
    if (root->key.id > id)
        return student_avl_search(root->lchild, id);
    else
        return student_avl_search(root->rchild, id);
}

void student_clearAVL(student_avl_node *root)
{
    if(root == STU_NIL) return ;
    student_clearAVL(root->lchild);
    student_clearAVL(root->rchild);
    free(root);
}

void student_inOrder(student_avl_node *root)
{
    if (root == STU_NIL)
        return;

    student_inOrder(root->lchild);

    printf("| %-4d | %-3d | %-5d | %-23s | %-6s | %-17s | %-9d |\n",
           root->key.id, root->key.age, root->key.grade, root->key.name,
           root->key.gender, root->key.enrollmentDate, root->key.classId);

    student_inOrder(root->rchild);
}

#endif

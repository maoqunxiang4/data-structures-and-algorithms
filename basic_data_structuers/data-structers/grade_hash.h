#ifndef Grade_Hash_H
#define Grade_Hash_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"
#include "../../user_data_structers/user_data_structers.h"
#include "../../utils/constants.h"

#define hash_swap(a, b) { \
    __typeof(a) __c = a; \
    a = b, b = __c; \
}

typedef struct GradeNode {
    Grade grade;
    struct GradeNode *next;
} GradeNode;

typedef struct GradeHashTable {
    GradeNode *data;
    int count, size;
} GradeHashTable;
 
typedef struct GradeHashTableSingleton {
    GradeHashTable *gradeHashTable ;
} GradeHashTableSingleton ;

void hashTableExpand(GradeHashTable *hashTable);
void clearGradeHashTable(GradeHashTable *hashTable);
GradeHashTable *getNewGradeHashTable(int size) ;

GradeHashTable *getGradeHashTableSingleton() {
    static GradeHashTableSingleton gradeHashTableSingleton ;
    gradeHashTableSingleton.gradeHashTable = getNewGradeHashTable(grade_hash_table_size) ;
    return gradeHashTableSingleton.gradeHashTable;
}

GradeNode *getNewGradeNode(const Grade *grade) {
    GradeNode *node = (GradeNode *)malloc(sizeof(GradeNode));
    node->grade = *grade;
    node->next = NULL;
    return node;
}

GradeHashTable *getNewGradeHashTable(int size) {
    GradeHashTable *hashTable = (GradeHashTable *)malloc(sizeof(GradeHashTable));
    hashTable->data = (GradeNode *)malloc(sizeof(GradeNode) * size);
    hashTable->size = size;
    hashTable->count = 0;
    for(int i = 0 ; i < hashTable->size ; i++) {
        hashTable->data[i].next = NULL ;
    }
    return hashTable;
}

int hashFunction(const char *studentName) {
    int seed = 131, hash = 0;
    for (int i = 0; studentName[i]; i++) {
        hash = hash * seed + studentName[i];
    }
    return hash & 0x7fffffff;
}

int findInGradeHashTable(GradeHashTable *hashTable, const char *studentName) {
    int hashCode = hashFunction(studentName), index = hashCode % hashTable->size;
    GradeNode *currentNode = hashTable->data[index].next;
    while (currentNode) {
        if (strcmp(currentNode->grade.studentName, studentName) == 0) {
            return 1;
        }
        currentNode = currentNode->next;
    }
    return 0;
}

int insertGradeNode(GradeHashTable *hashTable, const Grade *grade) {
    if (hashTable->count >= hashTable->size * 2) {
        hashTableExpand(hashTable);
    }
    int hashCode = hashFunction(grade->studentName), index = hashCode % hashTable->size;
    GradeNode *newNode = getNewGradeNode(grade);
    newNode->next = hashTable->data[index].next;
    hashTable->data[index].next = newNode;
    hashTable->count += 1;
    return 1;
}

void swapGradeHashTables(GradeHashTable *hashTable1, GradeHashTable *hashTable2) {
    hash_swap(hashTable1->data, hashTable2->data);
    hash_swap(hashTable1->count, hashTable2->count);
    hash_swap(hashTable1->size, hashTable2->size);
    return;
}

void hashTableExpand(GradeHashTable *hashTable) {
    printf("Expand Hash Table %d -> %d\n", hashTable->size, hashTable->size * 2);
    GradeHashTable *newHashTable = getNewGradeHashTable(hashTable->size * 2);
    for (int i = 0; i < hashTable->size; i++) {
        GradeNode *currentNode = hashTable->data[i].next;
        while (currentNode) {
            insertGradeNode(newHashTable, &(currentNode->grade));
            currentNode = currentNode->next;
        }
    }
    swapGradeHashTables(hashTable, newHashTable);
    clearGradeHashTable(newHashTable);
    return;
}

void clearGradeNode(GradeNode *node) {
    if (node == NULL) return;
    free(node);
    return;
}

void clearGradeHashTable(GradeHashTable *hashTable) {
    if (hashTable == NULL) return;
    for (int i = 0; i < hashTable->size; i++) {
        GradeNode *currentNode = hashTable->data[i].next, *nextNode;
        while (currentNode) {
            nextNode = currentNode->next;
            clearGradeNode(currentNode);
            currentNode = nextNode;
        }
    }
    free(hashTable->data);
    free(hashTable);
    return;
}

void showAllGradesInHashTable(GradeHashTable *hashTable) {
    printf("+-----------------------+----------------+------------------+-------+\n");
    printf("|    Student Name       |  Student ID    |   Course Name    | Score |\n");
    printf("+-----------------------+----------------+------------------+-------+\n");

    for (int i = 0; i < hashTable->size; i++) {
        GradeNode *currentNode = hashTable->data[i].next;
        while (currentNode) {
            printf("| %-21s | %-14d | %-16s | %.2f  |\n",
                   currentNode->grade.studentName, currentNode->grade.studentId,
                   currentNode->grade.coursename, currentNode->grade.score);
            currentNode = currentNode->next;
        }
    }

    printf("+-----------------------+----------------+------------------+-------+\n\n\n\n");
}

GradeNode* getAllGradesInHashTable(GradeHashTable *hashTable) {
    GradeNode *head = NULL;
    GradeNode *tail = NULL;

    for (int i = 0; i < hashTable->size; i++) {
        GradeNode *currentNode = hashTable->data[i].next;
        while (currentNode) {
            // 创建新节点，复制数据
            GradeNode *newNode = getNewGradeNode(&(currentNode->grade));

            // 连接到结果链表
            if (head == NULL) {
                head = newNode;
                tail = newNode;
            } else {
                tail->next = newNode;
                tail = newNode;
            }

            currentNode = currentNode->next;
        }
    }

    return head; // 返回链表的实际头节点
}


#endif
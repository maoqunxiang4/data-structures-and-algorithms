#ifndef Stack_H
#define Stack_H
#define STACK_DATA_MAX 1000
#define STACK_MAX 5
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

typedef struct stack_node {
    char *data ;
    struct stack_node *next ;
}stack_node ;

typedef struct stack_linkList {
    stack_node head ,*tail ;
}stack_linkList ;

typedef struct Stack {
    stack_linkList *list ;
    int cnt ;
    stack_node *top ;
}Stack ;

stack_node *stack_getNewNode(char *data) {
    stack_node *node = (stack_node *)malloc(sizeof(stack_node)) ;
    node->next = NULL ;
    node->data = strdup(data) ;
    return node ;
}

Stack *getStack() {
    //生成栈
    Stack *stack = (Stack *)malloc(sizeof(Stack)) ;
    stack->cnt = 0 ;

    //生成链表
    stack_linkList *linkList = (stack_linkList *)malloc(sizeof(stack_linkList)) ;
    //初始化head，tail指向head.next
    linkList->head.data = strdup("");
    linkList->head.next = NULL ;
    linkList->tail = stack_getNewNode("") ; 
    linkList->tail->next = linkList->head.next ;

    //其实我完全没有必要写这一段代码，因为我在这一段中没有完全理解malloc的作用是什么，
    //malloc是在需要额外存储数据的时候动态分配内存，如果我不需要额外存储数据，就不需要
    // stack->top = (stack_node*)malloc(sizeof(stack_node)) ;
    //我在一开始的时候直接就写成了stack->top = stack->list->head.next;，这就会导致内存泄漏情况的产生
    //初始的时候指向NULL
    stack->top = stack->list->head.next ;

    stack->list = linkList ;
    return stack ;
}

void insertNode(Stack *stack ,stack_node *node) {
    node->next = stack->list->head.next ;
    stack->list->head.next = node ;
    stack->cnt ++ ;
    stack->top = stack->list->head.next ;
}

void eraseNode(Stack *stack ) {
    stack_node *node = stack->list->head.next ;
    stack->list->head.next = node->next ;
    stack->cnt -- ;
    stack->top = stack->list->head.next ;
    free(node) ;
}

void push_stack(Stack *stack  ,stack_node *node) {
    if(stack!=NULL) {
        insertNode(stack ,node) ;
    }
}

int isEmpty(Stack *stack ) {
    return stack==NULL || stack->cnt==0 ;
}

void pop_stack(Stack *stack ) {
    if(stack==NULL || isEmpty(stack)) return ;
    //linkList.head连接下一个，cnt--
    eraseNode(stack) ;
}

void top_stack(Stack *stack ) {
    if(stack==NULL || isEmpty(stack)) return ;
    printf("%s\n" ,stack->top->data) ;
}

void clearStack(Stack *stack ) {
    if(stack==NULL) return ;
    //清理所有的栈节点
    stack_node *node = stack->list->head.next ;
    while(node) {
        stack_node *p = node->next ;
        free(node) ;
        node = p ;
    }
    //清理tail节点
    free(stack->list->tail) ;
    //清除链表
    free(stack->list) ;
    //清除栈
    free(stack) ;
}

#endif
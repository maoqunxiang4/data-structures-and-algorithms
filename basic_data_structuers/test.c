#include "../utils/constants.h"
#include "../model/log_model.h"

// 模拟日志记录
void simulateLogging(queue_singleton *singleton, const char *username, const char *command) {
    log_node *data = getLogNode(username, command);
    log_enqueue(singleton->queue_log, data);
}

int main() {
    // 初始化队列单例
    queue_singleton *singleton = getQueueSingleton();

    // 模拟记录日志
    simulateLogging(singleton, "user1", "command1");
    simulateLogging(singleton, "user2", "command2");
    simulateLogging(singleton, "user3", "command3");
    simulateLogging(singleton, "user4", "command4");
    simulateLogging(singleton, "user5", "command5");

    write_to_log_file(singleton->queue_log) ;

    // 显示日志
    show_Log(singleton->queue_log);

    // 释放队列内存
    log_freeQueue(singleton->queue_log);

    return 0;
}
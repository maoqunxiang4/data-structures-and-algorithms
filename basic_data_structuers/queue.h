#ifndef Queue_H
#define Queue_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

typedef struct queue_node {
    char *data ;
    struct queue_node *next ;
} queue_node ;

typedef struct queue_linkList {
    queue_node head ,*tail ;
}queue_linkList ;

typedef struct Queue{
    queue_linkList *linkList ;
    int cnt ;
} Queue ;

Queue *getQueue() ;
Queue *queue = getQueue() ;

queue_node *queue_getNewNode(char *data) {
    queue_node *node = (queue_node *)malloc(sizeof(queue_node)) ;
    node->data = strdup(data);
    node->next = NULL ;
    return ;
}

Queue *getQueue() {
    if(queue!=NULL) return queue ;
    Queue *new_queue = getQueue() ;
    new_queue->cnt = 0 ;

    //初始化链表
    new_queue->linkList = (queue_linkList *)malloc(sizeof(queue_linkList)) ;
    new_queue->linkList->head.next = NULL ;
    new_queue->linkList->head.data = '\0' ;

    new_queue->linkList->tail = queue_getNewNode('\0') ;
    new_queue->linkList->tail->next = queue->linkList->head.next ;
    
    queue = new_queue ;
    return queue ;
}

int isEmpty() {
    return queue->cnt == 0 || queue == NULL ;
}

void insert(queue_node *node) {
    if(queue==NULL) return ;
    queue->linkList->tail->next = node ;
    queue->cnt ++ ;
}

void erase() {
    if(queue==NULL || isEmpty()) return ;
    queue_node *node = queue->linkList->head.next ;
    queue->linkList->head.next == node->next ;
    queue->cnt -- ;
}

void push(queue_node *node) {
    if(queue == NULL) return ;
    insert(node) ;
}

void pop() {
    if(isEmpty()) return ;
    erase() ;
}

void clearQueue() {
    if(queue==NULL) return ;
    queue_node *node = queue->linkList->head.next ;
    while(node) {
        queue_node *p = node->next ; 
        free(node) ;
        node = p ;
    }

    free(queue->linkList->tail) ;
    free(queue->linkList) ;
    free(queue) ;
}

#endif
#ifndef Hash_H
#define Hash_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#define HASH_DATA_MAX 20

typedef struct hash_node {
    char *key ;
    struct hash_node *next ;
} hash_node ;

typedef struct HashList {
    hash_node *data ;
    int size ,cnt ;
} HashList ;

HashList *getHashList () ;
HashList * hashList = getHashList() ;

hash_node *hash_getNewNode(char* key) {
    hash_node *node = (hash_node *)malloc(sizeof(hash_node)) ;
    node->next = NULL ;
    node->key = key ;
    return node ;
}

HashList *getHashList (int size) {
    HashList *hashList = (HashList *)malloc(sizeof(HashList)) ;
    hashList->data = (hash_node *)malloc(sizeof(hash_node) * HASH_DATA_MAX) ;
    hashList->size = size ;
    hashList->cnt = 0 ;
    return hashList ;
}

int hash_func(char *str) {
    int hash_key = 0 ,seed = 188 ;
    for(int i = 0 ; str[i] ; i++ ) {
        hash_key = seed * str[i] ;
    }
    return hash_key;
}

void swap(void *a,void *b,int size) {
    void *temp = malloc(size) ;
    memcpy(temp ,b ,size) ; 
    memcpy(b , a ,size) ;
    memcpy(a , temp ,size) ; 
    return ;
}

void swapHashListData(HashList *h1 ,HashList *h2) {
    int data_size = sizeof(h1->data[0]) *(h1->size > h2->size ? h1->size : h2->size) ;

    swap(&(h1->cnt) ,&(h2->cnt) ,sizeof(h1->cnt)) ;
    swap(&(h1->size) ,&(h2->size) ,sizeof(h1->size)) ;
    swap(h1->data ,h2->data ,data_size) ;
    return ;
}

void expend() {
    //创建一个新的hash表
    HashList *new_hashList = getHashList() ;

    //插入数据（注意，这里不能单纯地指向原来的节点，因为hash_key发生了变化）
    for(int i = 0 ; i < hashList->size ; i++){
        hash_node *p = hashList->data[i].next ;
        while(p) {
            insert(new_hashList ,p->key) ;
            p = p->next ;
        }
    }

    //新旧hash表交换数值
    swapHashListData(hashList ,new_hashList) ;
    return ;
}

void insert(HashList *_hashList , char *key) {
    if(_hashList==NULL) return ;
    if(_hashList->cnt >= _hashList->size*2) expend()  ;
    int hash_key = hash_func(key) , index = hash_key % _hashList->size ;
    hash_node *node = hash_getNewNode(key) ;
    hash_node *p = _hashList->data[index].next ;
    _hashList->data[index].next = node ;
    node->next = p ;
    _hashList->size ++ ;
    return ;
}

hash_node *search(char *str) {
    //找到hash_key，随后遍历链表
    if(hashList==NULL) return ;
    int hash_key = hash_func(str) , index = hash_key % hashList->size ;
    hash_node *p = hashList->data[index].next ;
    while( p && strcmp(str ,p->key) ) {
        p = p->next ;
    }
    return p;
}

void clearHashList(HashList *_hashList) {
    for(int  i =  0 ; i < _hashList->size ; i++) {
        hash_node *p = _hashList->data[i].next ;
        while(p) {
            hash_node *q = p->next ;
            free(p) ;
            p = q ;
        }
    }
    free(_hashList->data) ;
    free(_hashList) ;
    return ;
}

#endif
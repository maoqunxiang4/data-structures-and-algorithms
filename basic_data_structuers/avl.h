#ifndef AVL_H
#define AVL_H
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

typedef struct avl_node {
    char *key ;
    int height ;
    struct avl_node *lchild ,*rchild ;
} avl_node ;

avl_node __NIL ;
#define NIL (&__NIL)
void initNIL() {
    NIL->key = '\0' ;
    NIL->height = 0 ;
    NIL->lchild = NIL ;
    NIL->rchild = NIL ;
}

#define H(n) (n->height)
#define R(n) (n->rchild)
#define L(n) (n->lchild)

avl_node *avl_getNewNode(char *key) {
    avl_node *node = (avl_node *)malloc(sizeof(avl_node)) ;
    node->key = strdup(key) ;
    node->height = 1 ;
    node->lchild = NIL ;
    node->rchild = NIL ;
    return node;
}

avl_node *predecessor(avl_node *root) {
    avl_node *p = root ;
    while(p!=NIL) {
        p = p->rchild ;
    }
    return p ;
}

void swap(char *str1 ,char *str2) {
    char *temp = (char *)malloc(sizeof(char) * 100 ) ;
    temp = strdup(str1) ;
    str1 = strdup(str2) ;
    str2 = strdup(temp)  ;
}

void updateHeight(avl_node *root) {
    H(root) = H(R(root->rchild)) >= H(L(root->lchild)) ? H(R(root->rchild)) + 1 : H(L(root->lchild)) + 1 ;
}

avl_node *left_rotate(avl_node *root) {
    avl_node *p = root->rchild ;
    root->rchild = p ->lchild ;
    p->lchild = root ;
    updateHeight(root) ;
    updateHeight(p) ;
    return p ;
}

avl_node *right_rotate(avl_node *root) {
    avl_node *p = root->lchild ;
    root->lchild = p ->rchild ;
    p->rchild = root ;
    updateHeight(root) ;
    updateHeight(p) ;
    return p ;
}

avl_node  *maintain(avl_node *root) {
    if(abs(H(R(root)) - H(L(root)) <= 1 )) return root ;
    else {
        //冲突发生了
        //L子树
        if(H(L(root)) > H(R(root))) {
            //LR型
            if(H(R(L(root))) > H(L(L(root))) ) {
                root->lchild = left_rotate(root->lchild) ;
            }
            root = right_rotate(root) ;
        }

        //R子树
        if(H(R(root)) > H(L(root))) {
            //RL型
            if(H(L(R(root))) > H(R(R(root))) ) {
                root->rchild = left_rotate(root->rchild) ;
            }
            root = right_rotate(root) ;
        }
    }
    return root ;
}

avl_node *insert(avl_node *root ,char *key) {
    //这一段一开始的时候还是有问题的，主要原因还是因为我没有理解算法的主要过程是什么
    // if(root==NIL) return getNewNode(key);
    // if(strcmp(root->key ,key) < 0 ) root->lchild = insert(root->lchild ,key ) ;
    // else if(strcmp(root->key ,key) > 0 ) root->rchild = insert(root->rchild , key ) ;
    // else return ;
    // return root ;
    if(root==NIL) return avl_getNewNode(key) ;
    else if (!strcmp(root->key ,key)) return root ;
    else if (strcmp(root->key ,key) < 0) root->lchild = insert(root->lchild ,key) ; 
    else if (strcmp(root->key ,key) > 0) root->rchild = insert(root->rchild ,key) ;
    updateHeight(root) ;
    return maintain(root) ;
}

avl_node *erase(avl_node *root ,char *key) {
    if(root==NIL) return root ;
    if(strcmp(root->key ,key) > 0) root->lchild = erase(root->lchild ,key) ;
    else if(strcmp(root->key ,key) < 0) root->rchild = erase(root->rchild ,key) ;
    else {
        //如果是出度为0或者出度为1的点：出度为0直接删除；出度为1，子节点直接接入祖父节点中
        if(root->lchild == NIL || root->rchild == NIL) {
            avl_node *p  =  root->lchild != NIL ? root->lchild : root->rchild ; 
            free(root) ;
            //直接接入到祖父节点中，在祖父节点中平衡
            return p ;
        }
        //如果是出度为2的点：转化为出度为1的节点
        else {
            avl_node *p = predecessor(root->lchild) ; 
            root->key = p->key ;
            root->lchild = erase(root->lchild ,key) ;
        }
    }
    updateHeight(root) ;
    return maintain(root) ;
}

int search(avl_node *root ,char *key) {
    if(strcmp(root->key ,key) == 0) return 1 ;
    if(strcmp(root->key ,key) < 0) return search(root->lchild ,key) ;
    else return search(root->rchild ,key) ;
    return ;
}

void clearAVL(avl_node *root) {
    if(root==NIL) return ;
    clearAVL(root->lchild) ;
    clearAVL(root->rchild) ;
    clearAVL(root) ;
}

void inOrder(avl_node *root) {
    if(root==NIL) return ;
    inOrder(root->lchild) ;
    inOrder(root) ;
    inOrder(root->rchild) ;
}

#endif
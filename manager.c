#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./service/grade_service.h"
#include "./service/student_service.h"
#include "./service/teacher_service.h"
#include "./service/user_service.h"
#include "./service/course_service.h"
#include "./service/user_service.h"
#include "./service/auth_service.h"

queue_singleton *queue_instance ;
queue_log *queue_read_log ;
GradeHashTable *grade_hash_table;
student_avl_node *stu_root;
teacher_avl_node *tea_root;
course_avl_node *cou_root;
SkipList* skipList ;
int is_admin = -1;

UserNode *usernode;
char *temp[20] ;

void clearScreen() {
    system("cls"); // Adjust based on your OS (e.g., "clear" for Unix-like systems)
}

void student_menu() {
    printf("\n+---------------------------------------------+");
    printf("\n|      Student Information Management Menu    |");
    printf("\n+---------------------------------------------+");
    printf("\n| 1. Add Student                              |");
    printf("\n| 2. Delete Student                           |");
    printf("\n| 3. Modify Student Information              |");
    printf("\n| 4. Show Student Information                |");
    printf("\n| 5. Search Student Information              |");
    printf("\n| 0. Back to Main Menu                        |");
    printf("\n+---------------------------------------------+\n");
}

void teacher_menu() {
    printf("\n+---------------------------------------------+");
    printf("\n|      Teacher Information Management Menu    |");
    printf("\n+---------------------------------------------+");
    printf("\n| 1. Add Teacher                              |");
    printf("\n| 2. Delete Teacher                           |");
    printf("\n| 3. Modify Teacher Information              |");
    printf("\n| 4. Show Teacher Information                |");
    printf("\n| 5. Search Teacher Information              |");
    printf("\n| 0. Back to Main Menu                        |");
    printf("\n+---------------------------------------------+\n");
}

void grade_menu() {
    printf("\n+---------------------------------------------+");
    printf("\n|           Grade Management Menu             |");
    printf("\n+---------------------------------------------+");
    printf("\n| 1. Add Grade                                |");
    printf("\n| 2. Delete Grade                             |");
    printf("\n| 3. Modify Grade                             |");
    printf("\n| 4. Search Grade                             |");
    printf("\n| 5. show Grade                               |");
    // printf("\n| 6. show School Grade                        |");
    printf("\n| 6. show Person All                          |");
    // printf("\n| 8. show Class Grade                         |");
    printf("\n| 7. show Grade Sort                          |");
    printf("\n| 0. Back to Main Menu                        |");
    printf("\n+---------------------------------------------+\n");
}

void course_menu() {
    printf("\n+---------------------------------------------+");
    printf("\n|      Course Information Management Menu    |");
    printf("\n+---------------------------------------------+");
    printf("\n| 1. Add Course                              |");
    printf("\n| 2. Delete Course                           |");
    printf("\n| 3. Modify Course Information               |");
    printf("\n| 4. Show Course Information                 |");
    printf("\n| 5. Search Course Information               |");
    printf("\n| 0. Back to Main Menu                       |");
    printf("\n+---------------------------------------------+\n");
}

void log_menu() {
    printf("\n+---------------------------------------------+");
    printf("\n|      Log Information Management Menu       |");
    printf("\n+---------------------------------------------+");
    printf("\n| 1. show Log                                |");
    printf("\n+---------------------------------------------+\n");
}

void log_management() {
    int log_id = -1;

    while (1) {
        log_menu();
        int choice;
        printf("\nEnter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                clearScreen();
                read_from_log_file(queue_read_log ,queue_read_log->logArr) ;
                break;

            case 0:
                return;
            default:
                clearScreen();
                printf("\nInvalid choice. Please try again.\n");
        }
    }
}


void course_management() {
    int course_id = -1;

    while (1) {
        course_menu();
        cour_root = initCourseInfo();

        int choice;
        printf("\nEnter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                clearScreen();
                // if(auth_hasPermission(skipList, "Admin", "High Operation"))
                    cour_root = addNewCourse();
                break;
            case 2:
                clearScreen();
                // if(auth_hasPermission(skipList, "Admin", "High Operation")) {
                    printf("\nEnter the course ID you want to delete: ");
                    scanf("%d", &course_id);
                    cour_root = deleteCourse(course_id);
                // }
                break;
            case 3:
                clearScreen();
                // if(auth_hasPermission(skipList, "Admin", "High Operation")) {
                    printf("\nEnter the course ID you want to change: ");
                    scanf("%d", &course_id);
                    cour_root = changeCourseInfo(cour_root, course_id);
                // }

                break;
            case 4:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "Low Query")) {
                    showAllCoursesInfo();
                    printf("input a word to continue ");
                    scanf("%s", temp);
                // }

                break;
            case 5:
                clearScreen();
                // if(auth_hasPermission(skipList, "Student", "Low Query")) {
                    printf("\nEnter the course ID you want to search: ");
                    scanf("%d", &course_id);
                    showCourseInfo(searchCourse(cour_root ,course_id)) ;
                    printf("input a word to continue ");
                    scanf("%s", temp);
                // }
                
                break;
            case 0:
                return;
            default:
                clearScreen();
                printf("\nInvalid choice. Please try again.\n");
        }
    }
}

void student_management() {
    int student_id = -1;
    Student student;

    while (1) {
        student_menu();
        student_root = initStudentInfo();

        int choice;
        printf("\nEnter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query"))
                    stu_root = addNewStudent();
                break;
            case 2:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    printf("\nEnter the student ID you want to delete: ");
                    scanf("%d", &student_id);
                    stu_root = deleteStudent(student_id);
                // }

                break;
            case 3:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    printf("\nEnter the student ID you want to change: ");
                    scanf("%d", &student_id);
                    stu_root = changeStudentInfo(student_id);
                // }
                break;
            case 4:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                   showStudentsInfo();
                // }
                printf("input a word to continue ") ;
                scanf("%s" ,temp) ;
                break;
            case 5:
                clearScreen();
                // if (auth_hasPermission(skipList, "Student", "Low Query")) {
                    printf("\nEnter the student ID you want to search: ");
                    scanf("%d", &student_id);
                    Student student = searchStudent(student_root, student_id);
                    showStudentInfo(student) ;
                    printf("input a word to continue ") ;
                    scanf("%s" ,temp) ;
                // }
                break;
            case 0:
                return;
            default:
                clearScreen();
                printf("\nInvalid choice. Please try again.\n");
        }
    }
}

void teacher_management() {
    int teacherId;
    while (1) {
        teacher_menu();
        int choice;
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                clearScreen();
                // if(auth_hasPermission(skipList, "Admin", "High Operation")) 
                    tea_root = addNewTeacher();
                break;
            case 2:
                clearScreen();
                // if(auth_hasPermission(skipList, "Admin", "High Operation"))  {
                    printf("Enter the Teacher ID to delete: ");
                    scanf("%d", &teacherId);
                    tea_root = deleteTeacher(teacherId);
                // }
                break;
            case 3:
                clearScreen();
                // if(auth_hasPermission(skipList, "Admin", "High Operation"))  {
                    printf("Enter the Teacher ID to modify: ");
                    scanf("%d", &teacherId);
                    tea_root = changeTeacherInfo(teacherId);
                // }
                break;
            case 4:
                clearScreen();
                // if(auth_hasPermission(skipList, "Admin", "High Operation"))  {
                    showAllTeachersInfo();
                    printf("input a word to continue ") ;
                    scanf("%s" ,temp) ;
                // }
                break;
            case 5:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    printf("Enter the Teacher ID to search: ");
                    scanf("%d", &teacherId);
                    Teacher teacher = searchTeacher(teacherId);
                    showTeacherInfo(teacher) ;
                    printf("input a word to continue ") ;
                    scanf("%s" ,temp) ;
                // }
                break;
            case 0:
                return;
            default:
                clearScreen();
                printf("Invalid choice. Please try again.\n");
        }
    }
}

void grade_management() {
    char studentName[50];
    int studentId;
    char coursename[50];
    int gradeId ;
    int classId ;
    while (1) {
        grade_menu();

        int choice;
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                clearScreen();
                addNewGrade();
                break;
            case 2:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    printf("Enter the Student Name , Student ID and Course Name to delete: ");
                    scanf("%s %d %s", studentName, &studentId, coursename);
                    deleteGrade(*gradeHashTable, studentName, studentId, coursename);
                // }
                break;
            case 3:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    printf("Enter the Student Name , Student ID and Course Name to modify: ");
                    scanf("%s %d %s", studentName, &studentId, coursename);
                    changeGradeInfo(*gradeHashTable, studentName, studentId, coursename);
                // }
                break;
            case 4:
                clearScreen();
                // if(auth_hasPermission(skipList, "Student", "Low Query")) {
                    printf("Enter the Student Name , Student ID and Course Name to search: ");
                    scanf("%s %d %s", studentName, &studentId, coursename);
                    searchGrade(*gradeHashTable, studentName, studentId, coursename);
                    printf("input a word to continue ") ;
                    scanf("%s" ,temp) ;
                // }
                break;
             case 5:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    printf("input gradeId \n") ;
                    showGradeHashTableData(gradeHashTable);
                    printf("input a word to continue ") ;
                    scanf("%s" ,temp) ;
                // }
                break;
            //  case 6:
            //     clearScreen();
            //     // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
            //         scanf("%d" ,&gradeId) ;
            //         displaySchoolStats(stu_root ,gradeId);
            //         printf("input a word to continue ") ;
            //         scanf("%s" ,temp) ;
            //     // }
            //     break;
             case 6:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    printf("input studentName \n") ;
                    scanf("%s",studentName) ;
                    printPersonGrades(findGradesForPerson(studentName));
                    printf("input a word to continue ") ;
                    scanf("%s" ,temp) ;
                // }
                break;
            // case 8:
            //     clearScreen();
            //     // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
            //         printf("input gradeId and classId \n") ;
            //         scanf("%d %d" ,&gradeId ,&classId) ;
            //         displayClassStats(stu_root ,gradeId ,classId);
            //         printf("input a word to continue ") ;
            //         scanf("%s" ,temp) ;
            //     // }
            //     break;
            case 7:
                clearScreen();
                // if(auth_hasPermission(skipList, "Teacher", "High Query")) {
                    displayGradesDescending();
                    printf("input a word to continue ") ;
                    scanf("%s" ,temp) ;
                // }
                break;
            case 0:
                return;
            default:
                clearScreen();
                printf("Invalid choice. Please try again.\n");
        }
    }
}

void main_menu() {
    stu_root = initStudentInfo() ;
    tea_root = initTeacherInfo() ;
    cou_root =  initCourseInfo() ;
    grade_hash_table = initGradeHashTable() ;
    
    queue_instance = getQueueSingleton() ;
    getGradeHashTableSingleton() ;
    getCourseSingleton() ;
    getTeacherSingleton() ;

    setCourseQueueInstance() ;
    setGradeQueueinstance() ;
    setStudentQueueinstance() ;
    setTeacherQueueinstance() ;

    while (1) {
        printf("\n+---------------- Main Menu -----------------+");
        printf("\n| 1. Student Information Management           |");
        printf("\n| 2. Teacher Information Management           |");
        printf("\n| 3. Grade Management                         |");
        printf("\n| 4. Course Management                        |");
        if (is_admin) printf("\n| 5. User Management                          |");
        printf("\n| 6. Log Management                           |");
        printf("\n| 0. Exit                                     |");
        printf("\n+---------------------------------------------+");

        int choice;
        printf("\nEnter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                clearScreen();
                
                student_management();
                break;
            case 2:
                clearScreen();
                teacher_management();
                break;
            case 3:
                clearScreen();
                grade_management();
                break;
            case 4:
                clearScreen();
                course_management();
                break;
            case 5:
                    clearScreen();
                    // if(auth_hasPermission(skipList , "admin","Privileged"))
                        registerUser();
                break;
             case 6:
                    clearScreen() ;
                    // if(auth_hasPermission(skipList , "admin","Privileged"))
                        log_management() ;
                break;
            case 0:
                clearScreen();
                printf("Exiting the system. Goodbye!\n");
                exit(0);
            default:
                clearScreen();
                printf("Invalid choice. Please try again.\n");
        }
    }
}


int main() {
    queue_read_log = log_initQueue(200) ;
    users_head = initUsers() ;
    usernode = login();
    // skipList = createSkipList();
    // if(strcmp(usernode->user.role ,"student"))
    //     assignPermissions(skipList, "student", LOW_STUDENT_PERMISSIONS, sizeof(LOW_STUDENT_PERMISSIONS) / sizeof(auth));
    // if(strcmp(usernode->user.role ,"teacher"))
    //     assignPermissions(skipList, "teacher", LOW_TEACHER_PERMISSIONS, sizeof(LOW_TEACHER_PERMISSIONS) / sizeof(auth));
    // if(strcmp(usernode->user.role ,"admin"))
    //     assignPermissions(skipList, "admin", ADMIN_PERMISSIONS, sizeof(ADMIN_PERMISSIONS) / sizeof(auth));
    main_menu();
    write_to_log_file(queue_instance->queue_log) ;
    return 0;
}

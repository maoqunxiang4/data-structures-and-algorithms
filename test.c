#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "./service/grade_service.h"
#include "./service/student_service.h"
#include "./service/course_service.h"
#include "./service/teacher_service.h"

// 示例用法
int main() {
    setStudentQueueinstance() ;
    student_avl_node *stu = initStudentInfo() ;
    Student student = searchStudent(stu ,1) ;
    showStudentInfo(student) ;
}
